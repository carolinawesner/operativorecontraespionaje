package controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.io.Serializable;

import model.Espia;
import model.Operativo;

/**
 * Esta clase est� pensada para que el usuario interact�e directamente con ella.
 * Almacena la informaci�n de los esp�as ingresados por el usuario, y permite 
 * modificar toda la informaci�n respecto a ellos y a sus conexiones. 
 * @author Caro, Xime.
 *
 */
public class Datos implements Serializable {

	private static final long serialVersionUID = 1L;
	//contiene la informaci�n de todos los espias ingresados
	private Map<String, Espia> espias;  //Diccionario cuya clave es el nombre de un espia y su valor
										// es el objeto espia.
	private Operativo operativo;  		//operativo armado a partir de los datos ingresados
	
	/**
	 * Constructor.
	 */
	public Datos() {
		espias = new HashMap<String, Espia>();
		operativo = new Operativo();
	}
	/**
	 * Constructor a partir de un operativo pasado por par�metro
	 * @param o Operativo del que se obtiene la informaci�n para armar los dstos.
	 */
	private Datos(Operativo o) {
		if(o != null) {
			operativo = o;
			espias = new HashMap<String, Espia>();
			llenarEspias();
		}
	}
	
	/**
	 * Completa el diccionario de espias a partir del operativo.
	 */
	private void llenarEspias() {
		for(Espia espia : operativo.obtenerEspias()) {
			String nombre = espia.darNombre();
			espias.put(nombre, espia);
		}
	}

	/**
	 * Agrega el nombre de un espia pasado por par�metro al conjunto de nombres registrados.
	 * @param nombre String que corresponde al nombre del espia a registrar.
	 * @throws IllegalArgumentException si el nombre ingresado ya esta registrado.
	 */
	public void agregarDatoEspia(String nombre) {
		if (esEspiaRegistrado(nombre))
			throw new IllegalArgumentException("El nombre " + nombre + " ya fue ingresado.");
		else {
			Espia espia = new Espia(nombre);
			espias.put(nombre, espia);
			operativo.agregarEspia(espia);
		}
	}
	
	/**
	 * Eval�a si el nombre pasado por parametro est� registrado.
	 * @param nombre String que corresponde al nombre de un espia.
	 * @return true si el nombre esta registrado y false en caso contrario.
	 */
	public boolean esEspiaRegistrado(String nombre) {
		return espias.containsKey(nombre);
	}
	
	/**
	 * Elimina el esp�a pasado por par�metro del registro de esp�as.
	 * @param nombre corresponde al nombre del esp�a.
	 * @throws IllegalArgumentException si el nombre ingresado no esta registrado.
	 */
	public void eliminarEspiaRegistrado(String nombre) {
		if(!esEspiaRegistrado(nombre)) {
			throw new IllegalArgumentException("El esp�a " + nombre +
					" no se encuentra registrado.");
		}	
		Espia e = espias.get(nombre);
		operativo.eliminarEspia(e);
		espias.remove(nombre);
	}
	
	/**
	 * Agrega la relaci�n entre los nombres de los espias pasados por par�metro al registro, 
	 * con su probabilidad de intercepci�n correspondiente.
	 * @param espia1 String que correponde al primer espia a relacionar.
	 * @param espia2 String que correponde al segundo espia a relacionar.
	 * @param prob Double que corresponde a la probabilidad de intercepci�n 
	 * de la comunicaci�n de los espias por el enemigo.
	 * @throws IllegalArgumentException si alguno de los nombres no est� registrado o la probabilidad 
	 * de intercepci�n no pertenece al intervalo [0,1].
	 */
	public void agregarConexion(String espia1, String espia2, Double prob) {
		verificarRegistroDeEspias (espia1, espia2);
		
		if (espia1.equals(espia2))
			throw new IllegalArgumentException("Los nombres ingresados son igules");
		
		else if (!esProbabilidadCorrecta(prob))
			throw new IllegalArgumentException("La probabilidad de intercepci�n es incorrecta, "
					+ "debe pertenecer a [0,1]");
		
		else if (conexionRegistrada(espia1, espia2))
			throw new IllegalArgumentException("La comunicaci�n ya fue registrada");
		
		else {
			Espia e1 = espias.get(espia1);
			Espia e2 = espias.get(espia2);
			operativo.agregarPosibilidadEncuentro(e1, e2, prob);
		}
	}
	
	/**
	 * Elimina la relaci�n entre los espias pasados por par�metro.
	 * @param espia1 String que correponde al nombre del primer espia a relacionar.
	 * @param espia2 String que correponde al nombre del segundo espia a relacionar.
	 * @throws IllegalArgumentException si alguno de los nombres no est� registrado o son iguales. 
	 */
	public void eliminarConexion(String espia1, String espia2) {
		verificarRegistroDeEspias (espia1, espia2);
		if (espia1.equals(espia2))
			throw new IllegalArgumentException("Los nombres ingresados son igules");
		Espia e1 = espias.get(espia1);
		Espia e2 = espias.get(espia2);
		operativo.eliminarPosibilidadEncuentro(e1, e2);
		
	}
	
	/**
	 * Modifica la probabilidad de intercepci�n de los esp�as pasado por
	 * par�metro. La relaci�n entre dos esp�as debe estar registrada.
	 * @param espia1 String que correponde al primer espia de la relaci�n.
	 * @param espia2 String que correponde al segundo espia de la relaci�n.
	 * @param prob Double que corresponde a la probabilidad de intercepci�n 
	 * de la comunicaci�n de los espias por el enemigo.
	 * @throws IllegalArgumentException si alguno de los nombres no est� registrado,
	 * la relaci�n no esta establecida o la probabilidad de intercepci�n 
	 * no pertenece al intervalo [0,1].
	 */
	public void modificarProbabilidadDeIntercepcion(String espia1, String espia2, Double prob) {
		 if (!conexionRegistrada(espia1, espia2))
				throw new IllegalArgumentException("La comunicaci�n no fue registrada");
		 if (!esProbabilidadCorrecta(prob))
			throw new IllegalArgumentException("La probabilidad de intercepci�n es incorrecta, debe pertenecer a [0,1]");
			Espia e1 = espias.get(espia1);
			Espia e2 = espias.get(espia2);
			operativo.modificarProbabilidadIntercepcion(e1, e2, prob);
	}
	
	/**
	 * Verifica que los nombres pasados por par�metro esten registrados.
	 * Lanza excepci�n si alguno de los nombres no esta reristrado.
	 */
	private void verificarRegistroDeEspias (String espia1, String espia2) {
		if (!esEspiaRegistrado(espia1))
			throw new IllegalArgumentException( "El nombre " + espia1 + " no est� registrado");
		else if (!esEspiaRegistrado(espia2))
			throw new IllegalArgumentException( "El nombre " + espia2 + " no est� registrado");
	}
	
	/**
	 * Eval�a si los nombres de los espias pasados por parametro ya registran conexi�n.
	 * @param espia1 String que corresponde al nombre de un espia.
	 * @param espia2 String que corresponde al nombre de otro espia.
	 * @return true si los espias estan conectados y false en caso contrario.
	 */
	public boolean conexionRegistrada(String espia1, String espia2) {
		verificarRegistroDeEspias(espia1, espia2);
		Espia e1 = espias.get(espia1);
		Espia e2 = espias.get(espia2);
		return operativo.existeProbabilidadDeEncuentro(e1, e2);
	}
	
	/**
	 * Eval�a si la probabilidad pasada por par�metro pertenece al intervalo [0,1].
	 * @param p Double que corresponde a la probabilidad a evaluar.
	 * @return true si pertence al intervalo o false si no pertenece.
	 */
	private boolean esProbabilidadCorrecta(Double p) {
		return (p >= 0 && p <= 1);
	}
	
	/**
	 * Devuelve un conjunto con los nombres de los espias registrados.
	 * @return conjunto de String que corresponde a todos los nombres de los espias registrados.
	 */
	public Set<String> darNombresEspias(){
		return espias.keySet();
	}
	
	/**
	 * Devuelve un conjunto con los nombres de los espias que se relacionan con el nombre 
	 * del espia pasado por par�metro. 
	 * @param nombre String que corresponde al nombre del espia a evaluar sus relaciones.
	 * @return conjunto de String que corresponde a los nombres de todos los espias en relaci�n
	 * con el espia indicado.
	 */
	public Set<String> darVecinosDe(String nombre){
		if (!esEspiaRegistrado(nombre))
			throw new IllegalArgumentException( "El nombre " + nombre + " no est� registrado");
		else {
			Espia espia = espias.get(nombre);
			Set<Espia> espAlcanzables = operativo.espiasAlcanzables(espia);
			Set<String> nombresEspias = new HashSet<String>();
			
			for(Espia e : espAlcanzables)
				nombresEspias.add(e.darNombre());
			
			return nombresEspias;
		}
	}
	
	/**
	 * Devuelve la probabilidad de intercepci�n de los nombres de los espias pasados por par�metro.
	 * @param espia1 String que corresponde al nombre del primer espia.
	 * @param espia2 String que corresponde al nombre del segundo espia.
	 * @return Double que corresponde a la probabilidad de que el enemigo intercepte a 
	 * los espias indicados.
	 */
	public Double darProbIntercepcion(String espia1, String espia2) {
		verificarRegistroDeEspias (espia1, espia2);
		Espia e1 = espias.get(espia1); 
		Espia e2 = espias.get(espia2); 
		return operativo.obtenerProbabilidadDeIntercepcion(e1, e2);
	}
	
	/**
	 * Devuelve una cadena con los nombres de los espias y
	 * sus relaciones segun el algoritmo de Prim.
	 */
	public String comunicacionPrim() {
		return operativo.toStringPrim();
	}
	
	/**
	 * Devuelve una cadena con los nombres de los espias y
	 * sus relaciones segun el algoritmo de Kruskal
	 */
	public String comunicacionKruskal() {
		return operativo.toStringKruskal();
	}
	
	/**
	 * Devuelve una lista de arreglos de String con los nombres de los espias que 
	 * se deben comunicar para obtener el camino mas seguro segun el algorimo pasado
	 * por par�metro.
	 * @param algoritmo String que representa al algoritmo de Prim o Kruskal
	 * @return Lista de arreglos de String
	 */
	public List<String[]> listaDeDuplasNombres(String algoritmo){
		if(algoritmo.equals("Kruskal"))
			return operativo.espiasConectadosKruskal();
		else if(algoritmo.equals("Prim"))
			return operativo.espiasConectadosPrim();
		else
			throw new IllegalArgumentException( "Algoritmo no v�lido");
	}

	/**
	 * Evalua si no tiene espias registrados.
	 * @return true si no tiene espias registrados y false en caso contrario.
	 */
	public boolean isEmpty() {
		return espias.isEmpty();
	}

	/**
	 * Devuelve la coordenada correspondiente al nombre del espia pasado por par�metro.
	 * @param nombre del espia
	 * @return arreglo de double que representa la coordenada.
	 */
	public double[] obtenerCoordenadasEspia(String nombre) {
		return espias.get(nombre).getCoord();
	}

	/**
	 * Modifica la coordenada del espia cuyo nombre se pasa por par�metro.
	 * @param nombre del espia a modificar la coordenada.
	 * @param coord arreglo de double que corresponde a la coordenada a reemplazar.
	 */
	public void guardarCoordenadasEspia(String nombre, double[] coord) {
		espias.get(nombre).setCoord(coord);
	}

	/**
	 * Guarda los datos en un archivo.
	 * @return true si logro guardarlo y false en caso contrario.
	 */
	public boolean guardarDatos() {
		return operativo.guardarDatos();
	}

	/**
	 * Recupera los datos del ultimo operativo guardado.
	 * @return Datos a partir del operativo guardado. 
	 */
	public static Datos recuperarDatos() {
		Operativo o = Operativo.recuperarDatos();
		return new Datos(o);
	}
	
}
