package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Esta clase modela un Grafo con v�rtices de tipo G�nerico y aristas con un peso.
 * 
 * @param <T> tipo de objeto de sus v�rtices.
 * @author Caro, Xime
 */
public class Grafo <T> implements Serializable {

	private static final long serialVersionUID = 1L;
	private Map <T, Set<Arista<T>>> aristasPorVertice; //V�rtices con sus aristas correspondientes

	/**
	 * Crea un grafo vac�o.
	 */
	public Grafo() {
		aristasPorVertice = new HashMap<T, Set<Arista<T>>>();
	}
	
	/**
	 * Recibe un conjunto de aristas y crea un grafo correspondiente.
	 * Si el par�metro es nulo el grafo resultante es nulo.
	 * Si el par�metro es vac�o crea un grafo vac�o.
	 * @param aristas corresponden a las aristas del nuevo grafo.
	 */
	public Grafo(Set<Arista<T>> aristas) {
		if(aristas != null) {
			aristasPorVertice = new HashMap<T, Set<Arista<T>>>();
			if(!(aristas.isEmpty())) {
				for(Arista<T> a : aristas)
					this.agregarAristaYPeso(a.getVertice1(), a.getVertice2(), a.getPeso());	
			}
		}
	}
	
	/**
	 * Agrega un v�rtice no existente al grafo.
	 * @param i corresponde a un v�rtice de tipo gen�rico.
	 * @throws IllegalArgumentException si el grafo ya contiene al v�rtice.
	 */
	public void agregarVertice(T i) throws IllegalArgumentException{
		if (existeVertice(i))
			throw new IllegalArgumentException("El elemento ya existe");
		else {
			Set<Arista<T>> aristas = new HashSet<Arista<T>>();
			aristasPorVertice.put(i, aristas);
		}
	}
	
	/**
	 * Eval�a si existe el v�rtice pasado por par�metro.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice del grafo.
	 * @return true si el v�rtice esta registrado en el grafo y false en caso contrario.
	 */
	public boolean existeVertice(T i) {
		return aristasPorVertice.keySet().contains(i);
	}
	
	/**
	 * Elimina un v�rtice existente del grafo.
	 * @param i corresponde a un v�rtice de tipo gen�rico.
	 * @throws IllegalArgumentException si el grafo no contiene al v�rtice.
	 */
	public void eliminarVertice(T i) throws IllegalArgumentException{
		if (!existeVertice(i))
			throw new IllegalArgumentException("El elemento no existe");
		else {
			Set<Arista<T>> aristasVerticeAEliminar = aristasPorVertice.get(i);
			for(Arista<T> a : aristasVerticeAEliminar) {
				T j = a.darElOtroElementoDeArista(i);
				aristasPorVertice.get(j).remove(a);
			}
			aristasPorVertice.remove(i);
		}
	}
	
	/**
	 * Devuelve el tama�o del grafo.
	 * @return int que corresponde a la cantidad de v�rtices del grafo.
	 */
	public int tamanio(){
		return aristasPorVertice.size();
	}
		
	/**
	 * Agrega una arista inexistente al grafo. Si alguno de los v�rtices no existen,
	 * los registra y luego agrega la arista.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice de la arista.
	 * @param j objeto de tipo gen�rico que corresponde a otro v�rtice de la arista.
	 * @param p Double que corresponde al peso de la arista
	 * @throws IllegalArgumentException si la arista ya est� registrada.
	 */
	public void agregarAristaYPeso(T i, T j, Double p) throws IllegalArgumentException {
		
		if(!existeVertice(i)) agregarVertice(i);

		if(!existeVertice(j)) agregarVertice(j);
		
		if (existeArista(i, j))
			throw new IllegalArgumentException("La arista ya existe");
		
		verificarDistintos(i, j);
		Arista<T> a = new Arista<T>(i, j, p);
		aristasPorVertice.get(i).add(a);
		aristasPorVertice.get(j).add(a);
	}
	
	/**
	 * Elimina una arista existente del grafo.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice de la arista.
	 * @param j objeto de tipo gen�rico que corresponde a otro v�rtice de la arista.
	 * @throws IllegalArgumentException si la arista no esta registrada.
	 */
	public void eliminarArista(T i, T j) throws IllegalArgumentException {
		if (!existeArista(i, j)) {
			throw new IllegalArgumentException("La arista no existe");
		}
		verificarDistintos(i, j);
		Arista<T> a = obtenerArista(i, j);
		aristasPorVertice.get(i).remove(a);
		aristasPorVertice.get(j).remove(a);
	}
	
	/**
	 * Modifica el peso de una arista existente del grafo.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice de la arista.
	 * @param j objeto de tipo gen�rico que corresponde a otro v�rtice de la arista.
	 * @throws IllegalArgumentException si la arista no esta registrada.
	 */
	public void modificarPesoArista(T i, T j, double peso) throws IllegalArgumentException {
		if (!existeArista(i, j)) {
			throw new IllegalArgumentException("La arista no existe");
		}
		verificarDistintos(i, j);
		Arista<T> a = obtenerArista(i, j);
		a.setPeso(peso);
	}
	
	/**
	 * Eval�a si existe la arista que conecta los v�rtices recibidos por par�metro.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice de la arista.
	 * @param j objeto de tipo gen�rico que corresponde a otro v�rtice de la arista.
	 * @return true si la arista existe en el grafo y false en caso contrario.
	 * @throws IllegalArgumentException si los v�rtices no estan registrados o 
	 * son el mismo objeto.
	 */
	public boolean existeArista(T i, T j) throws IllegalArgumentException{
		if(!existeVertice(i) || !existeVertice(j) )
			throw new IllegalArgumentException("Los v�rtices deben estar registrados.");
		
		verificarDistintos(i, j);
		Arista<T> a = obtenerArista(i, j);
		return aristasPorVertice.get(i).contains(a) && aristasPorVertice.get(j).contains(a);
	}

	/**
	 * Verifica que los v�rtices pasados por par�metro son distintos.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice del grafo.
	 * @param j objeto de tipo gen�rico que corresponde a otro v�rtice del grafo.
	 * @throws IllegalArgumentException si los v�rtices son el mismo objeto.
	 */
	private void verificarDistintos(T i, T j)throws IllegalArgumentException{
		if( i.equals(j) )
			throw new IllegalArgumentException("No se permite conexion entre los mismos elementos");
	}
		
	/**
	 * Devuleve el peso de la arista correspondiente a los v�rtices pasados por par�metro.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice de la arista.
	 * @param j objeto de tipo gen�rico que corresponde al otro v�rtice de la arista.
	 * @return Double que correpsonde al peso de la arista.
	 */
	public Double pesoDeArista(T i, T j) {
		if (!existeVertice(i) || !existeVertice(j) ) {
			throw new IllegalArgumentException("Un elemento no existe");
		}
		else {
			Arista<T> a = obtenerArista(i, j);
			return a.getPeso();
		}
	}
	
	/**
	 * Devuelve la arista que corresponde a los v�rtices pasados por par�metro.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice de la arista.
	 * @param j objeto de tipo gen�rico que corresponde al otro v�rtice de la arista.
	 * @return Arista de tipo gen�rico que contiene a los v�rtices correspondientes.
	 */
	private Arista<T> obtenerArista(T i, T j) {
		for(Arista<T> a : this.aristasPorVertice.get(i) ) {
			if(a.verificarExtremos(i, j)) {
				return a;
			}
		}
		return null;
	}
		
	/**
	 * Devuelve el conjunto de vecinos de un v�rtice pasado por par�metro.
	 * @param i objeto de tipo gen�rico que corresponde a un v�rtice del grafo.
	 * @return Set de tipo gen�rico que corresponde al conjunto de 
	 * v�rtices conectados al v�rtice correspondiente.
	 * @throws IllegalArgumentException si el v�rtice no pertenece al grafo.
	 */
	public Set<T> vecinos(T i) throws IllegalArgumentException{
		if (!existeVertice(i)) {
			throw new IllegalArgumentException("El elemento no existe");
		}
		Set<T> vecinos = new HashSet<T>();
		for (Arista<T> a : aristasPorVertice.get(i))
			vecinos.add(a.darElOtroElementoDeArista(i));
		return vecinos;		
	}
		
	/**
	 * Devuelve el conjunto de v�rtices del grafo.
	 * @return Set de tipo gen�rico que corresponde al conjunto de v�rtices del grafo.
	 */
	public Set<T> darVertices() {
		return aristasPorVertice.keySet();
	}
		
	/**
	 * Devuelve un v�rtice aleatorio del grafo.
	 * @return v�rtice del grafo.
	 * @throws IllegalArgumentException si el grafo no tiene v�rtices (est� vac�o).
	 */
	public T darUnVertice() throws IllegalArgumentException {
		if (aristasPorVertice.keySet().isEmpty())
			throw new IllegalArgumentException("El grafo no tiene v�rtices.");
		
		for(T elem : aristasPorVertice.keySet())
			return elem;
		return null;				//No llega ac� ya que retorna algo antes.
	}
	
	/**
	 * Devuelve todas las aristas del grafo.
	 * @return ArrayList de aristas del grafo.
	 */
	public List<Arista<T>> getAristas() {
		List<Arista<T>> ret = new ArrayList<Arista<T>>();
		
		for(T elem : aristasPorVertice.keySet()) {
			for(Arista<T> arista : aristasPorVertice.get(elem)) {
				if(!ret.contains(arista))
					ret.add(arista);
			}
		}
		return ret;
	}
	
	/**
	 * Eval�a si el grafo no es conexo.
	 * @return true si el grafo no es conexo y false si es conexo.
	 */
	public boolean tieneAlgunVerticeSinConexion() {
		for (T vertice: aristasPorVertice.keySet()) {
			if (aristasPorVertice.get(vertice).isEmpty()) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString () {
		if(aristasPorVertice == null) return null;
		String s = "";
		for (T i : aristasPorVertice.keySet()) {
			s = s + "Elemento [" + i.toString() + "] ";
			s = s + "Se relaciona con " + this.vecinos(i).toString() +"\n";
		}
		return s;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		Grafo<T> other =  (Grafo<T>) obj;
		return verificarAristas(other) && Objects.equals(darVertices(), other.darVertices());
	}
	
	/**
	 * Recibe un grafo como par�metro y verifica que tenga 
	 * las mismas aristas que el grafo correpondiente.
	 */
	private boolean verificarAristas(Grafo<T> g) {
		List<Arista<T>> aristasGrafo = g.getAristas();
		if (aristasGrafo.size() != this.getAristas().size())
			return false;

		for (Arista<T> a : this.getAristas()) {
			if (!aristasGrafo.contains(a))
				return false;
		}
		return true;
	}
}


