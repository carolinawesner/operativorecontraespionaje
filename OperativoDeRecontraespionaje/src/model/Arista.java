package model;

import java.io.Serializable;
import java.util.Objects;

/**
 * Esta clase modela a una relación entre dos elementos, en grafos
 * llamada Arista.
 * Cada arista tiene dos extremos pero los extremos no tienen orden.
 * Es decir, una arista con extremos a y b puede ser igual a otra con
 * extremos b y a. Para que se cumpla la igualdad, por otra parte,
 * las aristas deben tener el mismo peso.
 * No puede existir una arista "ciclo", es decir, un elemento no
 * se puede conectar con si mismo, por lo que se tiene que 
 * vertice1 != vertice2 para toda Arista.
 *
 * @param <T> corresponde al tipo de objeto de sus extremos.
 * @author Caro, Xime.
 */
public class Arista<T> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private T vertice1;
	private T vertice2;
	private double peso;
								
	
	/**
	 * Crea una arista que conecta los elementos especificados, con su peso correspondiente.
	 * @param v1 elemento tipo T que corresponde al primer elemento de la arista. 
	 * @param v2 elemento tipo T que corresponde al segundo elemento de la arista. 
	 * @param peso double que corresponde al peso de la arista.
	 * @throws IllegalArgumentException si alguno de los elementos es nulo o v1.equals(v2).
	 */
	public Arista(T v1, T v2, double peso) {
		if(v1 == null || v2 == null)
			throw new IllegalArgumentException("Los vértices no pueden ser nulos.");
		if(v1.equals(v2))
			throw new IllegalArgumentException("Los extremos deben ser distintos."
					+ "Extremos ingresados: " + v1 + " " + v2);
			
		this.vertice1 = v1;
		this.vertice2 = v2;
		this.peso = peso;
	}
	
	/**
	 * Devuelve el primer elemento de la arista.
	 * @return elemento tipo T que corresponde a un componente de la arista.
	 */
	public T getVertice1() {
		return vertice1;
	}
		
	/**
	 * Devuelve el segundo elemento de la arista.
	 * @return elemento tipo T que corresponde a un componente de la arista.
	 */
	public T getVertice2() {
		return vertice2;
	}
	
	/**
	 * Devuelve el peso de la arista.
	 * @return double que corresponde al peso de la arista.
	 */
	public double getPeso() {
		return peso;
	}
	
	/**
	 * Cambia el peso de la arista por el ingresado.
	 * @param nuevoPeso corresponde al nuevo peso modificado
	 * de la arista.
	 */
	public void setPeso(double nuevoPeso) {
		peso = nuevoPeso;
	}
	
	/**
	 * Devuelve el elemento que forma parte de la arista y que conecta con el elemento que recibe 
	 * como parametro.
	 * @param v corresponde a un elemento de tipo T que forma parte de la arista.
	 * @return elemento T que compone la arista.
	 * @throws IllegalArgumentException si el elemento no forma parte de la arista.
	 */
	public T darElOtroElementoDeArista (T v) {
		if(!(v.equals(this.vertice1) || v.equals(this.vertice2))) {
			throw new IllegalArgumentException("El elemento no forma parte de la arista");
		}
		if (!v.equals(this.vertice1)) {
			return this.vertice1;
		}
		else {
			return this.vertice2;
		}
	}
	
	/**
	 * Verifica que los vértices de esta arista sean iguales a los vértices pasados.
	 * por parámetro.
	 * @param v1 un vertice a verificar.
	 * @param v2 otro vertice a verificar, distinto al anterior.
	 * @return true si son iguales, false en caso de que no lo sean.
	 */
	public boolean verificarExtremos(T v1, T v2) {
		if(v1.equals(v2))
			throw new IllegalArgumentException("Los extremos deben ser distintos."
					+ "Extremos ingresados: " + v1 + " " + v2);
		return (vertice1.equals(v1) ^ vertice1.equals(v2)) && (vertice2.equals(v1) ^ vertice2.equals(v2));
	}
	
	/**
	 * Retorna la representación en forma de String de este objeto.
	 */
	@Override
	public String toString() {
		return "[El elemento "+ this.vertice1.toString() + " conecta con el elemento " 
				+ this.vertice2 + ", con peso " + this.peso + "]";
	}
	
	/**
	 * Verifica que el objeto pasado por parámetro sea igual a esta
	 * instancia de Arista.
	 */
	@SuppressWarnings("unchecked")
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Arista<T> other =  (Arista<T>) obj;
		return verificarExtremos(other.vertice1, other.vertice2)
				&& Objects.equals(peso, other.peso);
	}
	

}
