package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/*
 * Esta clase implementa el Algoritmo de Prim para la obtenci�n de un 
 * �rbol Generador M�nimo (AGM) a partir de un grafo.
 */
public class Prim <T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private Grafo <T> grafo;           //Grafo del cual se obtienen los datos a manipular.
	
	//Datos para armar el AGM
	private LinkedList<T> vertices;    //Conjunto de v�rtices
	private Set<Arista<T>> aristas;    //Conjunto de aristas
	private int i;                     //N�mero de iteraciones
	
	/**
	 * Constructor de un objeto que ejecuta el algoritmo de Prim sobre
	 * el grafo recibido como par�metro.
	 * @param grafo es el grafo sobre el que se calcular� este algoritmo.
	 * @throws IllegalArgumentException si el grafo est� vac�o, es nulo, o no es conexo.
	 */
	public Prim(Grafo <T> grafo) {
		if (grafo==null)
			throw new IllegalArgumentException("El grafo no puede ser nulo!");
		if (grafo.tamanio() == 0)
			throw new IllegalArgumentException("El grafo no puede estar vacio");
		if (grafo.getAristas().isEmpty())
			throw new IllegalArgumentException("El grafo no tiene conexiones.");
		if (grafo.tieneAlgunVerticeSinConexion())
			throw new IllegalArgumentException("El grafo no tiene todos sus v�rtices conectados.");
		this.grafo = grafo;
	}
		
	/**
	 * Devuelve el grafo obtenido en la implementaci�n del Algoritmo de Prim.
	 * @return Grafo que es un �rbol Generador M�nimo del grafo correspondiente.
	 */
	public Grafo<T> AGM () {
		inicializarDatos();
		while (i <= grafo.tamanio()-1) { 
			elegirAristaConMenorPeso();
			i ++;
		}
		return new Grafo<T>(aristas);
	}
	
	/**
	 * Carga los datos que permiten iniciar la ejecuci�n del Algoritmo.
	 */
	private void inicializarDatos () {
		vertices = new LinkedList<T>();
		T v = grafo.darUnVertice();
		vertices.add(v); //agrega un vertice
		this.aristas = new HashSet<Arista<T>>();
		i = 1;
	}
	
	/**
	 * Recorre los v�rtices evaluados y elige la arista que tiene el menor peso.
	 * Luego la agrega en los registros. 
	 */	
	private void elegirAristaConMenorPeso() {
		T verticeMarcado = vertices.getFirst();
		T verticeNoMarcado = null;
		Double peso = null;
			
		for (T i: vertices){
			Set<T> vecinos = grafo.vecinos(i);
			for (T j: vecinos) {
				//evalua si el vertice vecino ya est� registrado
				if (!vertices.contains(j)) {
					Double p = grafo.pesoDeArista(i, j);
					if (peso == null || p < peso) {
						verticeNoMarcado = j;
						verticeMarcado = i;
						peso = p;
					}
				}
			}
		}
		//agrega el vertice nuevo
		vertices.add(verticeNoMarcado); 
		//agrega arista correspondiente
		Arista<T> a = new Arista<T> (verticeMarcado, verticeNoMarcado, 
				grafo.pesoDeArista(verticeNoMarcado,verticeMarcado));
		this.aristas.add(a);
	}
}
