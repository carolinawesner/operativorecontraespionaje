package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/*
 * Esta clase ejecuta el algoritmo de Kruskal y obtiene un �rbol generador m�nimo
 * de un grafo.
 * Informaci�n sobre las variables de instancia:
 * grafo:			 		Es el grafo sobre el cual se almacenan los siguientes datos:
   aristas:						Todas las aristas del grafo estan almacenadas en esta lista.	
   vertices:					Corresponde a todos los v�tices del grafo.		
   posicionesDePadres:			Para cada vertice(i), la posicion de su padre en 
   								la lista de vertices esta en posicionesDePadres(i)
   								
 * @author Caro, Xime.
 */
public class Kruskal<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	private Grafo<T> grafo;
	private List<Arista<T>> aristas;
	private List<T> vertices;
	private int[] posicionesDePadres;

	/**
	 * Crea un objeto que ejecuta el algoritmo de Kruskal sobre
	 * el grafo recibido como par�metro.
	 * @param g es el grafo sobre el que se calcular� este algoritmo.
	 * No hay ninguna restricci�n con respecto a g. Es decir, puede
	 * ser de cualquier tipo de objetos y puede ser nulo o vac�o.
	 */
	public Kruskal(Grafo<T> g) {
		grafo = g;
		
		if(g != null) {
			grafo = g;
			aristas = g.getAristas();
			vertices = new ArrayList<T>();
			cargarDatosIniciales();
		}
	}
	
	/**
	 * Carga los datos necesarios para ejecutar el algoritmo.
	 */
	private void cargarDatosIniciales() {
		Set<T> vertices = grafo.darVertices();
		posicionesDePadres = new int[vertices.size()];
		int i = 0;
		for(T elem : vertices) {
			this.vertices.add(elem);
			posicionesDePadres[i] = i;
			i++;
		}
	}

	/**
	 * Calcula el algortimo de Kruskal sobre el grafo que se especific� en el 
	 * constructor.
	 * @return un arbol generador minimo del grafo, siendo �ste tambien un grafo.
	 * Si el grafo proporcionado es nulo, este m�todo devolver� null.
	 * Si el grafo proporcionado es vac�o, este m�todo devolver� un grafo vac�o.
	 */
	public Grafo<T> AGM() {
		if(grafo == null)
			return null;
		
		ordenarAristas();
		Set<Arista<T>> aristasRet = new HashSet<Arista<T>>();
		System.out.println("POS PADRES " + mostrar(posicionesDePadres));
		
		for(Arista<T> arista : aristas) {
			//no estan en la misma componente conexa
			if(!find(arista.getVertice1(), arista.getVertice2())) {
				System.out.println("POS PADRES " + mostrar(posicionesDePadres));
				//las pongo en la misma componente conexa
				union(arista.getVertice1(), arista.getVertice2());
				
				//a�ado la arista
				aristasRet.add(arista);
			}
		}
		System.out.println("POS PADRES " + mostrar(posicionesDePadres));
		return new Grafo<T>(aristasRet);
	}
	
	private String mostrar(int[] a) {
		String ret = "[ ";
		for(int i = 0; i < a.length; i++)
			ret += a[i] + " ";
		ret += "]";
		return ret;
	}

	/*
	 * Este m�todo devuelve true si los dos elementos pasados
	 * por par�metro pertenecen a una misma componente conexa.
	 * Devuelve false en caso contrario.
	 */
	private boolean find(T elem1, T elem2) {
		return raiz(elem1).equals(raiz(elem2));
	}
	
	/**
	 * Obtiene la ra�z del elemento obtenido por par�metro.
	 * Guarda cada elemento intermedio en una colecci�n que
	 * modela a la componente conexa a la que elem pertenece.
	 * Luego, ejecuta un m�todo para que cada elemento de esta
	 * componente conexa apunte a la ra�z de la misma.
	 * @param elem es el elemento sobre el que se iniciar� el 
	 * algoritmo para encontrar a la ra�z de su componente conexa.
	 * @return la ra�z de la componente conexa.
	 */
	private T raiz(T elem) {
		Integer posicionPadre = obtenerPadre(elem);
		T padre = vertices.get(posicionPadre);
		Set<T> verticesDeComponenteConexa = new HashSet<T>();
		verticesDeComponenteConexa.add(elem);
		
		while(!elem.equals(padre)) {
			elem = padre;
			verticesDeComponenteConexa.add(elem);
			posicionPadre = obtenerPadre(elem);
			padre = vertices.get(posicionPadre);
		}
		
		//Optimizaci�n. Todos los vertices de una componente conexa apuntan a la raiz
		apuntarARaiz(verticesDeComponenteConexa, padre);
		return padre;
	}
	
	/**
	 * Este m�todo devuelve la posicion del padre de un elemento 
	 * perteneciente al grafo.
	 */
	private Integer obtenerPadre(T elem) {
		Integer posicion = vertices.indexOf(elem);
		return posicionesDePadres[posicion];
	}
	
	
	/**
	 * Este m�todo apunta cada elemento de una componente conexa a 
	 * la ra�z de la misma.
	 * @param verticesDeComponenteConexa es un conjunto de v�rtices
	 * pertenecientes a una misma componente conexa.
	 * @param raiz es el padre mayor de todos los elementos de 
	 * verticesDeComponenteConexa
	 */
	private void apuntarARaiz(Set<T> verticesDeComponenteConexa, T raiz) {
		int posRaiz = vertices.indexOf(raiz);
		for(T elem : verticesDeComponenteConexa) {
			int posElem = vertices.indexOf(elem);
			posicionesDePadres[posElem] = posRaiz;
		}
	}
	
	/*
	 * Este m�todo une dos componentes conexas apuntando la raiz
	 * de la menor a la raiz de la mayor.
	 */
	private void union(T elem1, T elem2) {
		//Obtengo padres
		T raiz1 = raiz(elem1);
		T raiz2 = raiz(elem2);
		
		//Obtengo posiciones de padres
		Integer posRaiz1 = vertices.indexOf(raiz1);
		Integer posRaiz2 = vertices.indexOf(raiz2);
		
		Integer posRaizCompMayor = posRaizComponenteConexaMayor(posRaiz1, posRaiz2);
		
		//Modifico las posiciones para que el padre de la componente conexa mayor sea padre
		//del menor
		if(posRaizCompMayor.equals(posRaiz1)) {
			posicionesDePadres[posRaiz2] = posRaiz1;
		} else {
			posicionesDePadres[posRaiz1] = posRaiz2;
		}
		
	}
	
	/*
	 * Este m�todo devuelve la posici�n de la raiz de la componente conexa
	 * que tiene m�s elementos. 
	 */
	private Integer posRaizComponenteConexaMayor(Integer posRaiz1, Integer posRaiz2) {
		int cantElemPos1 = 0;
		int cantElemPos2 = 0;
		
		for(int i = 0; i < vertices.size(); i++) {
			Integer padreActual = posicionesDePadres[i];
			if(padreActual.equals(posRaiz1))
				cantElemPos1++;
			if(padreActual.equals(posRaiz2))
				cantElemPos2++;
		}
		
		if(cantElemPos1 > cantElemPos2) return posRaiz1;
		else return posRaiz2;
	}

	/*
	 * Este m�todo ordena las aristas seg�n el peso de las mismas.
	 * Este ordenamiento es de menor a mayor.
	 */
	private void ordenarAristas() {
		//ordena las aristas segun el peso. 
		Collections.sort(aristas, new Comparator<Arista<T>>() {
			@SuppressWarnings("deprecation")
			@Override
			public int compare(Arista<T> a1, Arista<T> a2) {
				return new Integer(comparar(a1.getPeso(), a2.getPeso()));
			}
		});
	}
	
	/*
	 * Este m�todo compara dos n�meros.
	 * Devuelve 0 si son iguales, 1 si el primero es mayor 
	 * que el segundo y -1 si el segundo es mayor que el primero.
	 */
	private int comparar(double d1, double d2) {
		if(d1 == d2) return 0;
		if(d1 < d2) return -1;
		else return 1;
	}
}
