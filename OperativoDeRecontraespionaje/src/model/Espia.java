package model;

import java.io.Serializable;
import java.util.Objects;

/**
 * Esta clase modela a un esp�a que tiene un nombre.
 * La �nica restricci�n con respecto a sus datos es que el nombre del esp�a
 * no puede estar en blanco ni ser nulo.
 * 
 * @author Caro, Xime.
 */
public class Espia implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String nombre;
	private double[] coord;
	
	/**
	 * Crea un esp�a con la informaci�n pasada por par�metro.
	 * @param nombre: nombre del esp�a.
	 * @throws IllegalArgumentException si el nombre del espia 
	 * es nulo o est� en blanco.
	 */
	public Espia(String nombre) throws IllegalArgumentException {
		if(nombre == null || nombre.isBlank())
			throw new IllegalArgumentException("El nombre no puede estar vac�o ni en blanco");
			this.nombre = nombre;
	}
	
	/**
	 * Devuelve la coordenada del espia, si no tiene coordenada devuelve null.
	 * @return  double[] donde el primer elemento es la coordenada en el eje X y el segundo 
	 * en el eje Y.
	 */
	public double[] getCoord() {
		return coord;
	}

	/**
	 * Modifica la coordenada del espia por la pasada por par�metro.
	 * @param coord double[] donde el primer elemento es la coordenada en el eje X y el segundo 
	 * en el eje Y.
	 */
	public void setCoord(double[] coord) {
		this.coord = coord;
	}
	
	/**
	 * Devuelve el nombre del esp�a.
	 * @return String correspondiente al nombre del esp�a.
	 */
	public String darNombre() {
		return nombre;
	}
	
	@Override
	public String toString() {
		String salida = "Espia [Nombre: " + this.nombre  + "]";
		return salida;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Espia other = (Espia) obj;
		return  Objects.equals(nombre, other.nombre);
	}

}
