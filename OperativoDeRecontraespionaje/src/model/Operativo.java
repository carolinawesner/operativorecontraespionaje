package model;

import java.util.List;
import java.util.Objects;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * Esta clase modela un operativo de Esp�as. Estos esp�as se deben comunicar entre s�, 
 * y cada acci�n de comunicarse tiene una probabilidad de intercepci�n determinada 
 * por el usuario.
 * Este operativo utiliza unicamente objetos de tipo Grafo de Esp�a, por lo que, al dar detalles
 * sobre su implemetaci�n, no es recomendable utilizarla como clase en contacto con el
 * usuario.
 * @author Caro, Xime.
 */
public class Operativo implements Serializable {

	private static final long serialVersionUID = 1L;
	private Grafo <Espia> espias;
	
	/**
	 * Crea un Operativo sin esp�as.
	 */
	public Operativo() { 
		espias = new Grafo<Espia>();
	}
	
	/**
	 * Agrega un esp�a al operativo.
	 * @param espia es el esp�a a agregar.
	 * No debe haber sido a�adido anteriormente.
	 */
	public void agregarEspia(Espia espia) {
		espias.agregarVertice(espia);
	}
	
	/**
	 * Elimina un esp�a del operativo. 
	 * Este esp�a debe estar registrado.
	 * @param espia corresponde al esp�a registrado a eliminar.
	 */
	public void eliminarEspia(Espia espia) {
		if(!espias.existeVertice(espia))
			throw new IllegalArgumentException("El esp�a no est� registrado.");
		espias.eliminarVertice(espia);
	}
	
	/**
	 * A�ade una posibilidad de encuentro entre dos esp�as. Estos deben ser distintos.
	 * Si estos esp�as no se encuentran registrados, se los a�ade.
	 * Si ya existe una posibilidad de encuentro entre estos dos esp�as
	 * no se realiza nada.
	 * @param espia1 corresponde a uno de los esp�as a relacionar.
	 * @param espia2 corresponde a el otro esp�a a relacionar.
	 * @param probabilidadIntercepcion es un n�mero correspondiente a 
	 * la probabilidad de que intercepten a estos dos esp�as cuando se
	 * est�n comunicando.
	 */
	public void agregarPosibilidadEncuentro(Espia espia1, Espia espia2, double probabilidadIntercepcion) {
		if(!espias.existeArista(espia1, espia2))
			espias.agregarAristaYPeso(espia1, espia2, probabilidadIntercepcion);
	}
	
	/**
	 * Elimina la posibilidad de encuentro entre dos esp�as.
	 * @param espia1 corresponde a uno de los esp�as de la relaci�n.
	 * @param espia2 corresponde a el otro esp�a de la relaci�n.
	 * @throws IllegalArgumentException si uno o ambos esp�as no se 
	 * encuentran registrados.
	 */
	public void eliminarPosibilidadEncuentro(Espia espia1, Espia espia2) {
		if(!espias.existeArista(espia1, espia2)) {
			throw new IllegalArgumentException("La relaci�n entre " + espia1 + "y " 
		+ espia2 + " no est� registrada.");
		}
		
		espias.eliminarArista(espia1, espia2);
	}
	
	/**
	 * Modifica la probabilidad de intercepci�n de una posibilidad
	 * de encuentro entre dos esp�as.
	 * @param espia1 corresponde a uno de los esp�as de la relaci�n.
	 * @param espia2 corresponde a el otro esp�a de la relaci�n.
	 * @param double probabilidadIntercepcion corresponde a la probabilidad de intercepci�n
	 * en un encuentro entre dos esp�as.
	 * @throws IllegalArgumentException si la posibilidad de encuentro 
	 * no se encuentra registrada.
	 */
	public void modificarProbabilidadIntercepcion(Espia espia1, Espia espia2, 
			double probabilidadIntercepcion) {
		if(!espias.existeArista(espia1, espia2)) {
			throw new IllegalArgumentException("La relaci�n entre " + espia1 + "y " 
		+ espia2 + " no est� registrada.");
		}
		espias.modificarPesoArista(espia1, espia2, probabilidadIntercepcion);
	}
	
	/**
	 * Verifica si dos esp�as se pueden conectar.
	 * @param espia1 corresponde a uno de los esp�as de la relaci�n.
	 * @param espia2 corresponde a el otro esp�a de la relaci�n.
	 * @return true si existe, false si no.
	 */
	public boolean existeProbabilidadDeEncuentro(Espia espia1, Espia espia2) {
		return espias.existeArista(espia1, espia2);
	}
	
	/**
	 * Obtiene la probabilidad de intercepci�n de un posible 
	 * encuentro entre esp�as.
	 * @param espia1 corresponde a uno de los esp�as de la relaci�n.
	 * @param espia2 corresponde a el otro esp�a de la relaci�n.
	 * @return -1 si esta probabilidad no existe o un n�mero 
	 * positivo o 0 correspondiente a esta probabilidad si existe.
	 */
	public double obtenerProbabilidadDeIntercepcion(Espia espia1, Espia espia2) {
		List<Arista<Espia>> aristas = espias.getAristas();
		for(Arista<Espia> a : aristas)
			if(a.verificarExtremos(espia1, espia2))
				return a.getPeso();
		return -1;
	}
	
	/**
	 * Devuelve un conjunto de esp�as correspondientes a los
	 * esp�as con los que se puede comunicar el esp�a pasado
	 * por par�metro.
	 * @param espia corresponde al esp�a al que se le buscan 
	 * sus esp�as alcanzables.
	 * @return un conjunto de esp�as.
	 * @throws IllegalArgumentException si el esp�a no se 
	 * encuentra registrado.
	 */
	public Set<Espia> espiasAlcanzables(Espia espia) {
		if(!espias.existeVertice(espia)) {
			throw new IllegalArgumentException("El esp�a no se encuentra registrado."
							+ " Esp�a ingresado: " + espia);
		}
		
		return espias.vecinos(espia);
	}
	
	/**
	 * Calcula el �rbol generador m�nimo de este operativo
	 * mediante el algoritmo de Prim.
	 * @return un grafo de esp�as que tiene los mismos
	 * esp�as que este operativo, pero con las 
	 * menores posibilidades de encuentro.
	 */
	public Grafo<Espia> comunicacionSeguraPrim() {
		if (this.espias.tieneAlgunVerticeSinConexion()) {
			throw new IllegalArgumentException("Existen espias incomunicados dentro del operativo");
		}
		else {
			Prim<Espia> p = new Prim<Espia>(espias);
			return p.AGM();
		}
	}
	
	/**
	 * Calcula el �rbol generador m�nimo de este operativo
	 * mediante el algoritmo de Kruskal.
	 * @return un grafo de esp�as que tiene los mismos
	 * esp�as que este operativo, pero con las 
	 * menores posibilidades de encuentro.
	 */
	public Grafo<Espia> comunicacionSeguraKruskal() {
		if (this.espias.tieneAlgunVerticeSinConexion()) {
			throw new IllegalArgumentException("Existen espias incomunicados dentro del operativo");
		}
		else {
			Kruskal<Espia> k = new Kruskal<Espia>(espias);
			return k.AGM();}
	}
	
	/**
	 * Devuelve una cadena con los nombres de los espias y sus relaciones segun el 
	 * algoritmo de Kruskal.
	 * @return String 
	 */
	public String toStringKruskal() {
		return toString(this.comunicacionSeguraKruskal());
	}
	
	/**
	 * Devuelve una cadena con los nombres de los espias y sus relaciones segun el 
	 * algoritmo de Prim.
	 * @return String 
	 */
	public String toStringPrim() {
		return toString(this.comunicacionSeguraPrim());
	}
	
	/**
	 * Devuelve una cadena que narra como deben comunicarse los espias del grafo pasado
	 * por parametro con su probabilidad de intercepci�n correspondiente.
	 * @param g grafo de espias que representa al operativo.
	 * @return String con las relaciones entre los espias.
	 */
	private String toString(Grafo<Espia> g) {
		StringBuilder ret = new StringBuilder();
		Set<Arista<Espia>> conexionesMostradas = new HashSet<Arista<Espia>>();
		
		for (Espia e : g.darVertices()) {
			ret.append("El espia ").append(e.darNombre().toUpperCase()).append(" se debe comunicar con:\n");	
			for (Espia t : g.vecinos(e)) {
				Arista<Espia> a = new Arista<Espia>(e, t, g.pesoDeArista(e, t));
				if (!conexionesMostradas.contains(a))
					ret.append("	");
					ret.append(t.darNombre().toUpperCase()).append(" (probabilidad de intercepci�n de ")
					.append(a.getPeso()).append(")\n");
					conexionesMostradas.add(a);
			}
			ret.append("\n");
		}
		return ret.toString();
	}
	
	//METODOS PARA OBTENER NOMBRES RELACIONADOS
	/**
	 * Devuelve la lista de los nombres de los espias que se deben comunicar segun 
	 * el camino calculado con el algoritmo de Prim.
	 * @return lista de arreglos de String, cuyo primer elemento corresponde al nombre de
	 * un espia y el segundo elemento al de otro espia que presentan posibilidad de 
	 * comunicacion entre ellos.
	 */
	public List<String[]> espiasConectadosPrim(){
		return nombresGrafo(comunicacionSeguraPrim());
	}
	
	/**
	 * Devuelve la lista de los nombres de los espias que se deben comunicar segun 
	 * el camino calculado con el algoritmo de Kruskal.
	 * @return lista de arreglos de String, cuyo primer elemento corresponde al nombre de
	 * un espia y el segundo elemento al de otro espia que presentan posibilidad de 
	 * comunicacion entre ellos.
	 */
	public List<String[]> espiasConectadosKruskal(){
		return nombresGrafo(comunicacionSeguraKruskal());
	}
	
	/**
	 * Devuelve una lista de cadenas con los nombres relacionados de un grafo de Espias
	 * @param g Grafo de Espias del cual se obtiene los nombres.
	 * @return Lista de arreglos de dos String.
	 */
	private List<String[]> nombresGrafo(Grafo<Espia> g){
		List<String[]> nombres = new LinkedList<String[]>();
		for(Arista<Espia> a : g.getAristas()) {
			String espia1 = a.getVertice1().darNombre();
			String espia2 = a.getVertice2().darNombre();
			if (!estaRelacion(nombres,espia1,espia2)) {
				String [] relacion = {espia1, espia2};
				nombres.add(relacion);			
			}
		}
		return nombres;
	}
	
	/**
	 * Evalua si los nombres pasados por par�metro estan en la lista correspondiente
	 * @param lista de dos nombres relacionados
	 * @param n1 string que corresponde al primer nombre a evaluar.
	 * @param n2 string que corresponde al segundo nombre a evaluar.
	 * @return true si los dos nombres estan relacionados en la lista, de lo contrario devulve false.
	 */
	private boolean estaRelacion(List<String[]> lista, String n1, String n2) {
		String [] array1 = {n1, n2};
		String [] array2 = {n2, n1};
		return lista.contains(array1) || lista.contains(array2);
	}
	
	/**
	 * Guarda los datos del operativo en el archivo "ultimoOperativoGuardado.txt".
	 * @return true si los datos fueron guardados correctamente, de lo contrario retorna false.
	 */ 
	public boolean guardarDatos() {
		try {
			FileOutputStream fos = new FileOutputStream("ultimoOperativoGuardado.txt");
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeObject(this);
			out.close();
			return true;
			
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Retorna el operativo guardado en el archivo "ultimoOperativoGuardado.txt"
	 * @return Operativo recuperado del archivo o null si no logro repuperarlo
	 * o el archivo estaba vac�o.
	 */
	public static Operativo recuperarDatos() {
		try {
			FileInputStream fis = new FileInputStream("ultimoOperativoGuardado.txt");
			ObjectInputStream in = new ObjectInputStream(fis);
			Operativo op = (Operativo) in.readObject();
			in.close();
			return op;
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Devulve el conjunto de espias del operativo.
	 * @return Set de espias del operativo.
	 */
	public Set<Espia> obtenerEspias() {
		return espias.darVertices();
	}
	
	/**
	 * Devuelve la coordenada del espia pasado por par�metro.
	 * @param espia del cual se quiere obtener la coordenada.
	 * @return arreglo de double que representa a la coordenada del espia
	 */
	public double[] obtenerCoordenadasEspia(Espia espia) {
		return espia.getCoord();
	}
	
	/**
	 * Guarda la coordenada del espia coorrespondiente.
	 * @param espia al que se le quiere modificar su coordenada.
	 * @param coord arreglo de double que representa la coordenada reemplazar.
	 */
	public void guardarCoordenadasEspia(Espia espia, double[] coord) {
		espia.setCoord(coord);
	}
	
	/**
	 * Dos operativos son iguales si tienen los mismos espias y relaciones.
	 */
	@Override
	public boolean equals (Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operativo other = (Operativo) obj;
		return  Objects.equals(espias, other.espias);
	}

}
