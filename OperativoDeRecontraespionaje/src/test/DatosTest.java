package test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import controller.Datos;

/**
 * Esta clase implementa tests unitarios sobre la clase
 * Datos.
 * @author Caro, Xime.
 */
public class DatosTest {

	//AGREGAR NOMBRES DE ESPIAS
	@Test
	public void agregarNombresEspiasTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		d.agregarDatoEspia("Jose");
		
		Set<String> esperado = new HashSet<String>();
		esperado.add("Juan");
		esperado.add("Jose");
		
		assertTrue(d.darNombresEspias().equals(esperado));
		assertTrue(d.esEspiaRegistrado("Juan"));
	}

	@Test (expected = IllegalArgumentException.class)
	public void agregarNombresEspiasRepetidosTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		d.agregarDatoEspia("Juan");
	}
	
	//ELIMINAR NOMBRES DE ESPIAS
	@Test (expected = IllegalArgumentException.class)
	public void eliminarEspiaNoRegistradoTest() {
		Datos d = new Datos();
		d.eliminarEspiaRegistrado("Juan");
	}
	
	@Test
	public void eliminarEspiaRegistradoTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		d.eliminarEspiaRegistrado("Juan");
		
		assertTrue(d.darNombresEspias().isEmpty());
	}
	
	//CONECTAR ESPIAS
	@Test (expected = IllegalArgumentException.class)
	public void conectarEspiasInexistentesTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		d.agregarDatoEspia("Jose");
		d.agregarConexion("Mario", "Luis", 0.1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void conectarEspiasConPesoIncorrectoTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		d.agregarDatoEspia("Jose");
		d.agregarConexion("Juan", "Jose", 2.0);
	}
	
	@Test 
	public void conectarEspiasHappyPathTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		d.agregarDatoEspia("Jose");
		d.agregarConexion("Juan", "Jose", 0.5);
		
		Set<String> esperadoParaJuan = new HashSet<String>();
		esperadoParaJuan.add("Jose");
		
		Set<String> esperadoParaJose = new HashSet<String>();
		esperadoParaJose.add("Juan");
	
		assertTrue(d.darVecinosDe("Juan").equals(esperadoParaJuan));
		assertTrue(d.darVecinosDe("Jose").equals(esperadoParaJose));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void conectarEspiasConexionYaRegistradaTest() {
		Datos d = new Datos();

		d.agregarDatoEspia("Juan");
		d.agregarDatoEspia("Jose");

		d.agregarConexion("Juan", "Jose", 0.6);
		d.agregarConexion("Jose", "Juan", 0.6);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void conectarASiMismoTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		d.agregarConexion("Juan", "Juan", 0.2);
	}
	
	//ELIMINAR CONEXION
	@Test 
	public void eliminarConexionEspiasRegistradosTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		d.agregarDatoEspia("Pedro");
	
		d.agregarConexion("Juan", "Pedro", 0.2);
		d.eliminarConexion("Juan", "Pedro");
		
		Set<String> esperado = new HashSet<String>();
		assertTrue(d.darVecinosDe("Juan").equals(esperado));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void eliminarConexionEspiaNoRegistradoTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");

		d.eliminarConexion("Juan", "Pedro");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void eliminarConexionEspiaIgualesTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		d.eliminarConexion("Juan", "Juan");
	}
	
	//PROBABLIDAD DE INTERCEPCION
	@Test 
	public void probabilidadDeEspiasExistentesTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		d.agregarDatoEspia("Jose");
		d.agregarConexion("Juan", "Jose", 0.5);
		
		Double probEsperada = 0.5;
		assertEquals(probEsperada, d.darProbIntercepcion("Juan", "Jose"));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void probabilidadDeEspiasInexistentesTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		d.agregarDatoEspia("Jose");
		d.agregarConexion("Juan", "Jose", 0.5);
		d.darProbIntercepcion("Juan", "Luis");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void modificarProbabilidadIntercepcionNoRegistradaTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		d.agregarDatoEspia("Jose");
		d.modificarProbabilidadDeIntercepcion("Juan", "Jose", 10.0);
	}
	
	@Test
	public void modificarProbabilidadIntercepcionRegistradaTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		d.agregarDatoEspia("Jose");
		d.agregarConexion("Juan", "Jose", 1.0);
		d.modificarProbabilidadDeIntercepcion("Jose", "Juan", 0.5);
		
		assertTrue(0.5 == d.darProbIntercepcion("Juan", "Jose"));
	}

	//OBTENER VECINOS
	@Test (expected = IllegalArgumentException.class)
	public void darVecinosEspiaNoRegistradoTest() {
		Datos d = new Datos();
		d.darVecinosDe("Espia");
	}
	
	@Test
	public void darVecinosEspiaSinConexionesTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		assertTrue(d.darVecinosDe("Juan").isEmpty());
	}
	
	//DAR NOMBRES DE LOS ESPIAS
	@Test
	public void darNombresTest() {
		Datos d = new Datos();
		d.agregarDatoEspia("Juan");
		d.agregarDatoEspia("Jose");
		
		Set<String> esperado = new HashSet<String>();
		esperado.add("Juan");
		esperado.add("Jose");
		assertTrue(d.darNombresEspias().size() == esperado.size());
		assertTrue(d.darNombresEspias().contains("Juan") 
				&& d.darNombresEspias().contains("Jose"));
	}
	
	@Test
	public void darNombresDatoVacioTest() {
		Datos d = new Datos();
	
		Set<String> esperado = new HashSet<String>();
		assertEquals(d.darNombresEspias(), esperado);
	}
}
