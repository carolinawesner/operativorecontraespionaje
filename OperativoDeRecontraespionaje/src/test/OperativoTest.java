package test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import model.Espia;
import model.Operativo;

/**
 * Esta clase implementa tests unitarios sobre la clase Operativo
 * @author Caro, Xime.
 */
public class OperativoTest {

	Operativo operativo ;
	
	//AGREGAR ESPIA
	@Test
	public void agregarEspiaOperativoVacioTest() {
		Espia e = new Espia("Juan");
		operativo = new Operativo();
		operativo.agregarEspia(e);
		
		Operativo esperado = new Operativo();
		esperado.agregarEspia(e);
		assertEquals(operativo, esperado);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void agregarEspiaExistenteTest() {
		Espia e = new Espia("Juan");
		operativo = new Operativo();
		operativo.agregarEspia(e);
		operativo.agregarEspia(e);
	}
	
	//ELIMINAR ESPIA
	@Test (expected = IllegalArgumentException.class)
	public void eliminarEspiaOperativoVacioTest() {
		Espia e = new Espia("Juan");
		operativo = new Operativo();
		operativo.eliminarEspia(e);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void eliminarEspiaInexistenteTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Jose");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.eliminarEspia(e2);
	}
	
	@Test 
	public void eliminarEspiaExistenteOperativoConMasDeUnEspiaTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Jose");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.agregarEspia(e2);
		operativo.eliminarEspia(e1);
		
		Operativo esperado = new Operativo();
		esperado.agregarEspia(e2);
		assertEquals(operativo, esperado);
	}
	
	@Test 
	public void eliminarEspiaExistenteOperativoConUnEspiaTest() {
		Espia e1 = new Espia("Juan");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.eliminarEspia(e1);
	
		Operativo esperado = new Operativo();
		assertEquals(operativo, esperado);
	}
	
	//POSIBILIDAD DE ENCUENTRO
	@Test 
	public void agregarPosibilidadEncuentroEspiasExistentesTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Jose");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.agregarEspia(e2);
		operativo.agregarPosibilidadEncuentro(e1, e2, 0.1);
	
		assertTrue(operativo.existeProbabilidadDeEncuentro(e1, e2));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void agregarPosibilidadEncuentroUnEspiaInexistentesTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Jose");
		operativo = new Operativo();
		operativo.agregarEspia(e1);

		operativo.agregarPosibilidadEncuentro(e1, e2, 0.1);
	
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void eliminarPosibilidadEncuentroUnEspiaInexistentesTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Jose");
		operativo = new Operativo();
		operativo.agregarEspia(e1);

		operativo.eliminarPosibilidadEncuentro(e1, e2);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void eliminarPosibilidadEncuentroEspiasInexistentesTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Jose");
		Espia e3 = new Espia("Pedro");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		
		operativo.eliminarPosibilidadEncuentro(e2, e3);
	}

	@Test 
	public void eliminarPosibilidadEncuentroEspiasExistentesTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Jose");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.agregarEspia(e2);
		operativo.agregarPosibilidadEncuentro(e1, e2, 0.2);

		operativo.eliminarPosibilidadEncuentro(e1, e2);

		assertFalse(operativo.existeProbabilidadDeEncuentro(e1, e2));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void modificarPosibilidadEncuentroEspiasInexistentesTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Jose");
		operativo = new Operativo();
		operativo.agregarEspia(e1);

		operativo.modificarProbabilidadIntercepcion(e1, e2, 0.5);
	}
	
	@Test 
	public void modificarPosibilidadEncuentroEspiasExistentesTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Pedro");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.agregarEspia(e2);
		operativo.agregarPosibilidadEncuentro(e1, e2, 0.2);
		operativo.modificarProbabilidadIntercepcion(e1, e2, 0.5);
		
		assertTrue(operativo.obtenerProbabilidadDeIntercepcion(e1, e2) == 0.5);
	}
	
	@Test 
	public void posibilidadEncuentroExistentesTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Pedro");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.agregarEspia(e2);
		operativo.agregarPosibilidadEncuentro(e1, e2, 0.2);
		
		assertTrue(operativo.existeProbabilidadDeEncuentro(e1, e2));
	}
	
	@Test 
	public void posibilidadEncuentroInexistentesTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Pedro");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.agregarEspia(e2);
		
		assertFalse(operativo.existeProbabilidadDeEncuentro(e1, e2));
	}
	
	@Test 
	public void obtenerProbabilidadExistenteTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Pedro");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.agregarEspia(e2);
		operativo.agregarPosibilidadEncuentro(e1, e2, 0.2);
		
		assertTrue (operativo.obtenerProbabilidadDeIntercepcion(e1, e2) == 0.2);
	}
	
	@Test 
	public void obtenerProbabilidadInexistenteTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Pedro");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.agregarEspia(e2);
		
		assertTrue(operativo.obtenerProbabilidadDeIntercepcion(e1, e2) == -1);
	}
	
	//ESPIAS ALCANZABLES
	@Test (expected = IllegalArgumentException.class)
	public void espiasAlcanzablesDeEspiaInexistenteTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Pedro");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.espiasAlcanzables(e2);
	}
	
	@Test 
	public void espiasAlcanzablesDeEspiaExistenteTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Pedro");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.agregarEspia(e2);
		operativo.agregarPosibilidadEncuentro(e1, e2, 0.2);
		
		Set<Espia> esperado= new HashSet<Espia>();
		esperado.add(e2);
		assertEquals(operativo.espiasAlcanzables(e1), esperado);
	}
	
	//CONJUNTO DE ESPIAS DEL OPERATIVO
	@Test 
	public void obtenerEspiasTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Pedro");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.agregarEspia(e2);
		operativo.agregarPosibilidadEncuentro(e1, e2, 0.2);
		
		Set<Espia> esperado= new HashSet<Espia>();
		esperado.add(e1);
		esperado.add(e2);
		assertEquals(operativo.obtenerEspias(), esperado);
	}
	
	@Test 
	public void obtenerEspiasNuloTest() {
		operativo = new Operativo();
		Set<Espia> esperado= new HashSet<Espia>();
	
		assertEquals(operativo.obtenerEspias(), esperado);
	}
	
	//AGM PRIM Y KRUSKAL
	@Test
	public void AGMPrimKruskalDatosConDifProbTest() {
		operativo = HerramientasDeTest.operativoConProbabilidadesDiferentes();
		assertEquals(operativo.comunicacionSeguraKruskal(),operativo.comunicacionSeguraPrim());
	}
	
	@Test 
	public void AGMPrimKruskalOperativoProbabilidadesRepetidasTest() {
		operativo = HerramientasDeTest.operativoConProbabilidadesRepetidas();
		assertEquals(operativo.comunicacionSeguraKruskal(), operativo.comunicacionSeguraPrim());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void AGMPrimGrafoNoConexo() {
		operativo  = HerramientasDeTest.operativoGrafoNoConexo();
		operativo.comunicacionSeguraPrim();
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void AGMKruskalGrafoNoConexo() {
		operativo = HerramientasDeTest.operativoGrafoNoConexo();
		operativo.comunicacionSeguraKruskal();
	}
	
	//NOMBRES ESPIAS CONECTADOS
	@Test
	public void espiasConectadosTest() {
		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Pedro");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.agregarEspia(e2);
		operativo.agregarPosibilidadEncuentro(e1, e2, 0.2);
		
		assertTrue((operativo.espiasConectadosKruskal().get(0)[0].equals("Juan") &&
				operativo.espiasConectadosKruskal().get(0)[1].equals("Pedro")) || 
				operativo.espiasConectadosKruskal().get(0)[0].equals("Pedro") &&
				operativo.espiasConectadosKruskal().get(0)[1].equals("Juan"));
		
		assertTrue((operativo.espiasConectadosPrim().get(0)[0].equals("Juan") &&
				operativo.espiasConectadosPrim().get(0)[1].equals("Pedro")) || 
				operativo.espiasConectadosPrim().get(0)[0].equals("Pedro") &&
				operativo.espiasConectadosPrim().get(0)[1].equals("Juan"));
	}
	
	//COORDENADAS DE ESPIAS
	@Test
	public void obtenerCoordenadasExistentesTest() {
		Espia e1 = new Espia("Juan");
		double[] coord = {0.1, 0.2};
		e1.setCoord(coord);
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		
		assertEquals(operativo.obtenerCoordenadasEspia(e1), coord);
	}
	
	@Test
	public void obtenerCoordenadasNulaTest() {
		Espia e1 = new Espia("Juan");
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		
		assertEquals(operativo.obtenerCoordenadasEspia(e1), null);
	}
	
	@Test
	public void guardarCoordenadasTest() {
		Espia e1 = new Espia("Juan");
		double[] coord = {0.1, 0.2};
		operativo = new Operativo();
		operativo.agregarEspia(e1);
		operativo.guardarCoordenadasEspia(e1, coord);
		
		assertEquals(operativo.obtenerCoordenadasEspia(e1), coord);
	}
}
