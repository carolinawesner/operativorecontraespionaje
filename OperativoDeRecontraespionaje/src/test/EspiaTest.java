package test;

import static org.junit.Assert.*;
import org.junit.Test;
import model.Espia;

/**
 * Esta clase implementa tests unitarios sobre la clase
 * Espia.
 * @author Caro, Xime.
 */
public class EspiaTest {

	//NOMBRE
	@Test(expected = IllegalArgumentException.class)
	public void nombreVaciotest() {
		@SuppressWarnings("unused")
		Espia e = new Espia("");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void nombreEnBlancoTest() {
		@SuppressWarnings("unused")
		Espia e = new Espia("  ");
	}
	
	@Test
	public void obtenerNombreTest() {
		Espia e = new Espia("Espia");
		String expected = "Espia";
		assertEquals(expected, e.darNombre());
	}
	
	//EQUALS
	@Test
	public void equalsEspiasIgualesTest() {
		Espia e1 = new Espia("Nombre");
		Espia e2 = new Espia("Nombre");
		
		assertEquals(e1, e2);
	}
	
	@Test
	public void equalsEspiasDistintosTest() {
		Espia e1 = new Espia("Nombre");
		Espia e2 = new Espia("Nombre2");
		
		assertFalse(e1.equals(e2));
	}
	
	@Test
	public void equalsEspiaNuloTest() {
		Espia e1 = new Espia("Nombre");
		Espia e2 = null;
		
		assertFalse(e1.equals(e2));
	}
	
	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void equalsObjetosDistintos() {
		Espia espia = new Espia("Nombre");
		String nombre = "Nombre";
		
		assertFalse(espia.equals(nombre));
	}
	
	@Test
	public void equalsSobreSiMismoTest() {
		Espia e1 = new Espia("Nombre");
		
		assertEquals(e1, e1);
	}
	
	//TO STRING
	@Test
	public void toStringTest() {
		Espia e1 = new Espia("Agente 03");
		String st = "Espia [Nombre: Agente 03]";
		
		assertEquals(st, e1.toString());
	}
	
	//COORDENADAS
	@Test
	public void setCoordNulaTest() {
		Espia e1 = new Espia("Agente");
		double[] coord = {0.03, 0.05};
		e1.setCoord(coord);
		
		assertEquals(e1.getCoord(), coord);
	}
	
	@Test
	public void setCoordExistenteTest() {
		Espia e1 = new Espia("Agente");
		double[] coord1 = {0.03, 0.05};
		double[] coord2 = {0.03, 0.05};
		e1.setCoord(coord1);
		e1.setCoord(coord2);
		
		assertEquals(e1.getCoord(), coord2);
	}
	
	@Test
	public void getCoordNulaTest() {
		Espia e1 = new Espia("Agente");
		
		assertEquals(e1.getCoord(), null);
	}
	
	
}
