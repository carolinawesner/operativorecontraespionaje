package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Grafo;
import model.Prim;

public class PrimTest {
	
	/* Grafo
	 * 

	 1 -- 2 -- 5
	  \  / \  /    
	   3 --- 4  
	   
	*/

	//CASOS BORDE
	@SuppressWarnings("unused")
	@Test (expected = IllegalArgumentException.class)
	public void AGMGrafoNuloTest(){
		Grafo<Integer> grafo = new Grafo<Integer>();
		grafo = null;
		Prim<Integer> p = new Prim<Integer> (grafo);
	}
	
	@SuppressWarnings("unused")
	@Test (expected = IllegalArgumentException.class)
	public void AGMGrafoVacioTest(){
		Grafo<Integer> grafo = new Grafo<Integer>();
		Prim<Integer> p = new Prim<Integer> (grafo);
	}

	@SuppressWarnings("unused")
	@Test (expected = IllegalArgumentException.class)
	public void AGMGrafoSinAristasTest(){
		Grafo<Integer> grafo = new Grafo<Integer>();
		
		Prim<Integer> p = new Prim<Integer> (grafo);
	}
	
	@SuppressWarnings("unused")
	@Test (expected = IllegalArgumentException.class)
	public void AGMGrafoConAlgunVerticeSinAristaTest(){
		Grafo<Integer> grafo = HerramientasDeTest.grafoConPesosDistintos();
		grafo.eliminarArista(1, 2);
		grafo.eliminarArista(1, 3);
		Prim<Integer> p = new Prim<Integer> (grafo);
	}

	//OTROS CASOS
	@Test 
	public void AGMGrafoConDistintosPesosTest() {
		Grafo<Integer> grafo = HerramientasDeTest.grafoConPesosDistintos();
		Prim<Integer> p = new Prim<Integer> (grafo);
		Grafo<Integer> AGM = p.AGM();
		
		assertEquals(HerramientasDeTest.AGMGrafoConPesosDistintos(), AGM);
	}
	
	@Test 
	public void AGMGrafoConPesosIgualesTest() {
		Grafo<Integer> grafo = HerramientasDeTest.grafoDeIntegerConPesosRepetidos();
		Prim<Integer> p = new Prim<Integer> (grafo);
		Grafo<Integer> AGM = p.AGM();
		
		
		assertEquals(HerramientasDeTest.AGMdeGrafoConPesosRepetidos(), AGM);
	}
	
	@Test 
	public void AGMGrafoQueEsUnAGMTest() {
		Grafo<Integer> AGMInicial = HerramientasDeTest.AGMdeGrafoConPesosRepetidos();
		Prim<Integer> p = new Prim<Integer>(AGMInicial);
		
		Grafo<Integer> AGMObtenido = p.AGM();
		assertEquals(AGMInicial, AGMObtenido);	
	}
}
