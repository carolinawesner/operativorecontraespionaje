package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import model.Arista;
import model.Grafo;

/**
 * Esta clase implementa tests unitarios sobre la clase
 * Grafo.
 * @author Caro, Xime.
 */
public class GrafoTest {
	
	Grafo<Integer> g;
	
	//CONSTRUCTOR CON PAR�METROS
	@Test (expected = NullPointerException.class)
	public void crearGrafoAristasNulasTest()  {
		g = new Grafo<Integer>(null);
		g.getAristas(); 
	}
	
	@Test
	public void crearGrafoAristasVaciasTest() {
		Set<Arista<Integer>> aristas = new HashSet<Arista<Integer>>();
		g = new Grafo<Integer>(aristas);
		assertTrue(g.darVertices().isEmpty());
	}
	
	@Test
	public void crearGrafoHappyPathTest() {
		Set<Arista<Integer>> aristas = new HashSet<Arista<Integer>>();
		aristas.add(new Arista<Integer>(1, 2, 100));
		aristas.add(new Arista<Integer>(3, 2, 90));
		aristas.add(new Arista<Integer>(3, 7, 90));
		g = new Grafo<Integer>(aristas);
		
		Set<Integer> verticesExpected = new HashSet<Integer>();
		verticesExpected.add(1);
		verticesExpected.add(2);
		verticesExpected.add(3);
		verticesExpected.add(7);
		
		List<Arista<Integer>> aristasExpected = new ArrayList<Arista<Integer>>();
		aristasExpected.add(new Arista<Integer>(1, 2, 100));
		aristasExpected.add(new Arista<Integer>(3, 2, 90));
		aristasExpected.add(new Arista<Integer>(3, 7, 90));
		
		boolean estanTodas = true;
		for(Arista<Integer> a : g.getAristas())
			estanTodas = estanTodas && aristasExpected.contains(a);
		
		assertEquals(verticesExpected, g.darVertices());
		assertTrue(estanTodas);
	}

	//VERTICES
	@Test(expected = IllegalArgumentException.class)
	public void agregarVerticeExistenteTest() {
		g = new Grafo<Integer>();
		g.agregarVertice(2);
		g.agregarVertice(2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void eliminarVerticeInexistenteTest() {
		g = new Grafo<Integer>();
		g.agregarVertice(2);
		g.eliminarVertice(3);
	}
	
	@Test
	public void eliminarVerticeSinAristasHappyPathTest() {
		g = new Grafo<Integer>();
		g.agregarVertice(2);
		g.eliminarVertice(2);
		
		assertTrue(g.darVertices().isEmpty());
	}
	
	@Test
	public void eliminarVerticeConAristasTest() {
		g = new Grafo<Integer>();
		g.agregarAristaYPeso(2, 3, 10.1);
		g.agregarAristaYPeso(2, 5, 1.1);
		g.agregarAristaYPeso(2, 1, 59.3);
		g.agregarAristaYPeso(5, 1, 3.3);
		
		Set<Integer> verticesExpected = new HashSet<Integer>();
		verticesExpected.add(3);
		verticesExpected.add(5);
		verticesExpected.add(1);
		
		List<Arista<Integer>> aristasExpected = new ArrayList<Arista<Integer>>();
		aristasExpected.add(new Arista<Integer>(1, 5, 3.3));
		
		g.eliminarVertice(2);
		
		assertEquals(verticesExpected, g.darVertices());
		assertEquals(aristasExpected, g.getAristas());
		assertTrue(g.vecinos(3).isEmpty());
	}
	
	@Test
	public void obtenerVerticesHappyPathTest() {
		g = new Grafo<Integer>();
		g.agregarVertice(2);
		g.agregarVertice(4);
		g.agregarVertice(6);
		
		Set<Integer> expected = new HashSet<Integer>();
		expected.add(2);
		expected.add(4);
		expected.add(6);
		
		assertEquals(expected, g.darVertices());
	}
	
	@Test
	public void obtenerVerticesGrafoVacioTest() {
		g = new Grafo<Integer>();
		Set<Integer> expected = new HashSet<Integer>();
		assertEquals(expected, g.darVertices());
	}
	
	@Test
	public void darUnVerticeTest() {
		g = new Grafo<Integer>();
		g.agregarVertice(2);
		g.agregarVertice(3);
		g.agregarVertice(8);
		
		assertTrue(g.darVertices().contains(g.darUnVertice()));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void darUnVerticeGrafoSinVerticesTest() {
		g = new Grafo<Integer>();
		g.darUnVertice();
	}
	
	@Test
	public void algunVerticeSinConexionTest() {
		g = new Grafo<Integer>();
		g.agregarVertice(8);
		g.agregarAristaYPeso(2, 3, 0.2);
		assertTrue(g.tieneAlgunVerticeSinConexion());
	}
	
	@Test
	public void algunVerticeSinConexionGrafoConexoTest() {
		g = new Grafo<Integer>();
		g.agregarAristaYPeso(2, 3, 0.2);
		g.agregarAristaYPeso(7, 3, 150.1);
		g.agregarAristaYPeso(2, 9, 1.2);
		assertFalse(g.tieneAlgunVerticeSinConexion());
	}
	
	//ARISTAS
	@Test
	public void agregarAristaTest() {
		g = new Grafo<Integer>();
		g.agregarAristaYPeso(2, 9, 0.1);
		
		assertTrue(g.existeArista(2, 9));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaExistenteTest() {
		g = new Grafo<Integer>();
		g.agregarAristaYPeso(2, 9, 0.1);
		g.agregarAristaYPeso(2, 9, 0.2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void agregarAristaASiMismoTest() {
		g = new Grafo<Integer>();
		g.agregarVertice(2);
		g.agregarAristaYPeso(2, 2, 1.1);
	}
	
	@Test
	public void eliminarAristaTest() {
		g = new Grafo<Integer>();
		g.agregarAristaYPeso(2, 9, 2.1);
		g.eliminarArista(2, 9);
		
		assertFalse(g.existeArista(2, 9));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void modificarPesoAristaInexistenteTest() {
		g = new Grafo<Integer>();
		g.modificarPesoArista(2, 3, 0.0);
	}
	
	@Test
	public void modificarPesoAristaHappyPath() {
		g = new Grafo<Integer>();
		g.agregarAristaYPeso(2, 3, 0.0);
		g.modificarPesoArista(3, 2, 10);
		
		assertTrue(10 == g.pesoDeArista(2, 3));
	}
	
	@Test
	public void agregarAristaUnVerticeInexistenteTest() {
		g = new Grafo<Integer>();
		g.agregarVertice(2);
		g.agregarAristaYPeso(2, 9, 0.1);
		assertTrue(g.existeArista(2, 9));
	}
	
	@Test
	public void agregarAristaDosVerticesInexistentesTest() {
		g = new Grafo<Integer>();
		g.agregarAristaYPeso(2, 9, 1.1);
		assertTrue(g.existeArista(2, 9));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void eliminarAristaVerticesInexistentesTest() {
		g = new Grafo<Integer>();
		g.eliminarArista(2, 9);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void eliminarAristaNoRegistradaTest() {
		g = new Grafo<Integer>();
		g.agregarVertice(2);
		g.agregarVertice(9);
		g.eliminarArista(2, 9);
	}
	
	@Test
	public void aristaSimentricaTest() {
		g = new Grafo<Integer>();
		g.agregarVertice(2);
		g.agregarVertice(9);
		g.agregarAristaYPeso(2, 9, 0.1);
		
		assertTrue(g.existeArista(9, 2));
	}
	
	@Test
	public void getAristasHappyPathTest() {
		g = new Grafo<Integer>();
		g.agregarAristaYPeso(2, 9, 20.1);
		g.agregarAristaYPeso(3, 5, 50.2);
		g.agregarAristaYPeso(3, 9, 808.3);
		
		List<Arista<Integer>> expected = new ArrayList<Arista<Integer>>();
		expected.add(new Arista<Integer>(2, 9, 20.1));
		expected.add(new Arista<Integer>(3, 5, 50.2));
		expected.add(new Arista<Integer>(3, 9, 808.3));
		
		List<Arista<Integer>> aristasObt = g.getAristas();

		boolean estanTodas = true;
		for(Arista<Integer> a : aristasObt) 
			estanTodas = estanTodas && expected.contains(a);
		
		assertTrue(estanTodas);
		
	}
	
	//PESOS DE ARISTAS
	@Test
	public void pesoDeAristaHappyPathTest() {
		g = new Grafo<Integer>();
		g.agregarAristaYPeso(2, 9, 0.1);
		
		assertEquals("0.1", g.pesoDeArista(2, 9).toString());
	}
	
	@Test
	public void pesoDeAristaExtremosIntercambiadosTest() {
		g = new Grafo<Integer>();
		g.agregarAristaYPeso(2, 9, 200.1);
		
		assertEquals("200.1", g.pesoDeArista(9, 2).toString());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void pesoDeAristaInexistenteTest() {
		g = new Grafo<Integer>();
		g.pesoDeArista(3, 7);
	}
	
	
	//TAMA�O
	@Test
	public void tamanioTest() {
		g = new Grafo<Integer>();
		assertEquals(0, g.tamanio());
	}
	
	//VECINOS
	@Test
	public void darVecinosHappyPathTest() {
		g = new Grafo<Integer>();
		g.agregarAristaYPeso(2, 3, 0.1);
		g.agregarAristaYPeso(2, 4, 0.2);
		
		Set<Integer> expected = new HashSet<Integer>();
		expected.add(3);
		expected.add(4);
		
		assertEquals(expected, g.vecinos(2));
	}
	
	@Test
	public void darVecinosGrafoSinRelacionesTest() {
		g = new Grafo<Integer>();
		g.agregarVertice(2);
		g.agregarVertice(3);
		g.agregarVertice(4);
		
		Set<Integer> expected = new HashSet<Integer>();
		assertEquals(expected, g.vecinos(2));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void darVecinosVerticeInexistenteTest() {
		g = new Grafo<Integer>();
		g.vecinos(2);
	}
	
	//EQUALS
	@Test
	public void equalsMismoObjetoTest() {
		g = new Grafo<Integer>();
		g.agregarVertice(2);
		g.agregarVertice(3);
		g.agregarVertice(4);
		
		assertEquals(g, g);
	}
	
	@Test
	public void equalsGrafosIgualesTest() {
		g = new Grafo<Integer>();
		g.agregarVertice(2);
		g.agregarVertice(3);
		g.agregarVertice(4);
		
		Grafo<Integer> otroGrafo = new Grafo<Integer>();
		otroGrafo.agregarVertice(2);
		otroGrafo.agregarVertice(3);
		otroGrafo.agregarVertice(4);
		
		assertEquals(g, otroGrafo);
	}
	
	@Test
	public void equalsGrafosDistintosTest() {
		g = new Grafo<Integer>();
		g.agregarVertice(2);
		g.agregarVertice(3);
		
		Grafo<Integer> otroGrafo = new Grafo<Integer>();
		otroGrafo.agregarAristaYPeso(2, 3, 20.0);
		
		assertFalse(g.equals(otroGrafo));
	}
	
	@Test
	public void equalsGrafoAristasDistintasTest() {
		g = new Grafo<Integer>();
		g.agregarAristaYPeso(2, 3, 20.0);
		g.agregarAristaYPeso(2, 7, 21.0);
		
		Grafo<Integer> otroGrafo = new Grafo<Integer>();
		otroGrafo.agregarAristaYPeso(2, 3, 20.0);
		otroGrafo.agregarAristaYPeso(2, 7, 22.0);
		
		assertFalse(g.equals(otroGrafo));
	}
	
	@Test
	public void equalsObjetoNuloTest() {
		g = new Grafo<Integer>();
		assertFalse(g.equals(null));
	}
	
	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void equalsObjetoDeOtraClaseTest() {
		g = new Grafo<Integer>();
		g.agregarAristaYPeso(2, 3, 20.0);
		assertFalse(g.equals(new Arista<Integer>(2, 3, 20.0)));
	}

	
	
	

}
