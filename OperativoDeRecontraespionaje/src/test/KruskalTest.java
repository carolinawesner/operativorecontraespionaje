package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import model.Arista;
import model.Grafo;
import model.Kruskal;

/**
 * Esta clase implementa tests unitarios sobre la clase
 * Kruskal.
 * @author Caro, Xime.
 */
public class KruskalTest {
	Grafo<String> g;
	
	
	@Test
	public void AGMGrafoGrandeTest() {
		g = HerramientasDeTest.obtenerGrafoEjemploDiapositivas();
		List<Arista<String>> aristasEsp = HerramientasDeTest.obtenerAristasEsperadasGrafoEjDiapos();
		
		Kruskal<String> k = new Kruskal<String>(g);
		Grafo<String> AGM = k.AGM();
		List<Arista<String>> aristasAGM = AGM.getAristas();
		
		boolean contieneTodas = true;
		for(Arista<String> a : aristasAGM) {
			contieneTodas = contieneTodas && aristasEsp.contains(a);
		}
		
		assertTrue(contieneTodas);
	}
	
	
	@Test
	public void AGMGrafoChicoTest() {
		g = HerramientasDeTest.obtenerGrafoPocosVertices();
		Kruskal<String> k = new Kruskal<String>(g);
		
		Grafo<String> AGM = k.AGM();
		double pesoEsperado = 0.2 + 0.2 + 0.3 + 0.4;
		double pesoObtenido = 0;
		
		List<Arista<String>> aristasAGM = AGM.getAristas();
		for(Arista<String> a : aristasAGM)
			pesoObtenido += a.getPeso();
		
		assertTrue(pesoEsperado == pesoObtenido);
	}
	
	//CASOS BORDE
	@Test
	public void grafoNuloTest() {
		Kruskal<String> k = new Kruskal<String>(g);
		Grafo<String> AGM = k.AGM();
		assertNull(AGM);
	}
	
	@Test
	public void grafoVacioTest() {
		g = new Grafo<String>();
		Kruskal<String> k = new Kruskal<String>(g);
		Grafo<String> AGM = k.AGM();
		List<Arista<String>> expected = new ArrayList<Arista<String>>();
		assertEquals(expected, AGM.getAristas());
	}
	
	//CASOS ESPECÍFICOS
	@Test
	public void grafoDosVerticesUnaAristaTest() {
		g = new Grafo<String>();
		g.agregarAristaYPeso("A", "B", 1.0);
		
		Kruskal<String> k = new Kruskal<String>(g);
		Grafo<String> AGM = k.AGM();
		
		List<Arista<String>> aristaEsp = new ArrayList<Arista<String>>();
		aristaEsp.add(new Arista<String>("A", "B", 1.0));
		
		assertEquals(aristaEsp, AGM.getAristas());
	}
	
	@Test
	public void grafoDosVerticesSinAristaTest() {
		g = new Grafo<String>();
		g.agregarVertice("A");
		g.agregarVertice("B");
		
		Kruskal<String> k = new Kruskal<String>(g);
		Grafo<String> AGM = k.AGM();
		
		List<Arista<String>> aristasEsp = new ArrayList<Arista<String>>();
		assertEquals(aristasEsp, AGM.getAristas());
	}

}
