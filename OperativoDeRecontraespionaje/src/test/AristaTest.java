package test;

import static org.junit.Assert.*;
import org.junit.Test;

import model.Arista;

/**
 * Esta clase implementa tests unitarios sobre la clase
 * Arista.
 * @author Caro, Xime.
 */
public class AristaTest {

	//CREACION
	@Test (expected = IllegalArgumentException.class)
	public void aristaConAlgunElementoNuloTest() {
		@SuppressWarnings("unused")
		Arista<Integer> a = new Arista<Integer> (null,2,0.1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void aristaConElementosIgualesTest() {
		@SuppressWarnings("unused")
		Arista<Integer> a = new Arista<Integer> (2, 2, 0.1);
	}

	//OBTENER VERTICES
	@Test
	public void verticesHappyPathTest() {
		Arista<Integer> a = new Arista<Integer> (1, 2, 99.1);
		assertTrue(a.getVertice1() == 1);
		assertTrue(a.getVertice2() == 2);
	}
	
	@Test
	public void obtenerElOtroElementoDeAristaTest() {
		Arista<Integer> a = new Arista<Integer> (1, 2, 0.1);
		assertTrue(a.darElOtroElementoDeArista(2) == 1);
		assertTrue(a.darElOtroElementoDeArista(1) == 2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void obtenerOtroElementoDeElementoNoPertenecienteAristaTest() {
		Arista<Integer> a = new Arista<Integer>(1, 2, 0.1);
		a.darElOtroElementoDeArista(3);
	}
	
	//PESO
	@Test
	public void obtenerPesoTest() {
		Arista<Integer> a = new Arista<Integer> (1, 2, 179.18);
		boolean expected = 179.18 == a.getPeso();
		assertTrue(expected);
	}
	
	@Test
	public void modificarPesoTest() {
		Arista<Integer> a = new Arista<Integer> (1, 2, 19.0);
		a.setPeso(179.18);
		boolean expected = 179.18 == a.getPeso();
		assertTrue(expected);
	}
	
	//EQUALS
	@Test
	public void equalsHappyPathTest() {
		Arista<Integer> a1 = new Arista<Integer>(1, 2, 0.1);
		Arista<Integer> a2 = new Arista<Integer>(1, 2, 0.1);
		assertEquals(a1, a2);
	}
	@Test
	public void aristasIgualesConVerticesIntercambiadosTest() {
		Arista<Integer> a1 = new Arista<Integer>(1, 2, 0.1);
		Arista<Integer> a2 = new Arista<Integer>(2, 1, 0.1);
		assertEquals(a1, a2);
	}
	
	@Test
	public void equalsAristasConDosVerticesDistintosTest() {
		Arista<Integer> a1 = new Arista<Integer>(1, 3, 0.1);
		Arista<Integer> a2 = new Arista<Integer>(2, 4, 0.1);
		assertFalse(a1.equals(a2));
	}
	
	@Test
	public void equalsAristasConUnVerticeDistintoTest() {
		Arista<Integer> a1 = new Arista<Integer>(1, 3, 0.1);
		Arista<Integer> a2 = new Arista<Integer>(3, 4, 0.1);
		assertFalse(a1.equals(a2));
	}
	
	@Test
	public void equalsAristasConPesoDistintoTest() {
		Arista<Integer> a1 = new Arista<Integer> (1, 3, 0.1);
		Arista<Integer> a2 = new Arista<Integer> (1, 3, 0.4);
		assertFalse(a1.equals(a2));
	}
	
	@Test
	public void equalsAristaNulaTest() {
		Arista<Integer> a1 = new Arista<Integer>(1, 3, 0.1);
		Arista<Integer> a2 = null;
		assertFalse(a1.equals(a2));
	}
	
	@Test
	public void equalsMismoObjetoAristaTest() {
		Arista<Integer> a = new Arista<Integer>(1, 3, 0.1);
		assertEquals(a, a);
	}
	
	@SuppressWarnings("unlikely-arg-type")
	@Test
	public void equalsAristaYOtroObjetoTest() {
		Arista<Integer> a = new Arista<Integer>(1, 3, 0.1);
		assertFalse(a.equals(1));
	}

	//VERIFICAR EXTREMOS
	@Test(expected = IllegalArgumentException.class)
	public void verificarExtremosExtremosIgualesTest() {
		Arista<Integer> a = new Arista<Integer>(1, 3, 0.1);
		a.verificarExtremos(1, 1);
	}
	
	@Test
	public void verificarExtremosHappyPathTest() {
		Arista<Integer> a = new Arista<Integer>(1, 3, 0.1);
		assertTrue(a.verificarExtremos(1, 3));
	}
	
	@Test
	public void verificarExtremosExtremosIncorrectosTest() {
		Arista<Integer> a = new Arista<Integer>(1, 3, 0.1);
		assertFalse(a.verificarExtremos(7, 6));
	}
	
	//TO STRING
	@Test
	public void toStringTest() {
		Arista<Integer> a = new Arista<Integer>(1, 3, 0.1);
		String expected = "[El elemento 1 conecta con el elemento 3, con peso 0.1]";
		assertEquals(expected, a.toString());
	}
	
}
