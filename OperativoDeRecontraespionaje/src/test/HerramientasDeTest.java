package test;

import java.util.ArrayList;
import java.util.List;

import model.Arista;
import model.Espia;
import model.Grafo;
import model.Operativo;

public class HerramientasDeTest {

	//GRAFO<STRING>
	public static Grafo<String> obtenerGrafoEjemploDiapositivas() {
		Grafo<String> grafo = new Grafo<String>();
		grafo.agregarAristaYPeso("A", "B", 0.4);
		grafo.agregarAristaYPeso("A", "H", 0.8);
		grafo.agregarAristaYPeso("B", "H", 1.0);
		grafo.agregarAristaYPeso("H", "I", 0.6);
		grafo.agregarAristaYPeso("H", "G", 0.1);
		grafo.agregarAristaYPeso("B", "C", 1.0);
		grafo.agregarAristaYPeso("C", "I", 0.3);
		grafo.agregarAristaYPeso("I", "G", 0.5);
		grafo.agregarAristaYPeso("C", "F", 0.4);
		grafo.agregarAristaYPeso("G", "F", 0.3);
		grafo.agregarAristaYPeso("C", "D", 0.6);
		grafo.agregarAristaYPeso("D", "F", 1.0);
		grafo.agregarAristaYPeso("D", "E", 0.9);
		grafo.agregarAristaYPeso("F", "E", 1.0);
		
		return grafo;
	}
	
	public static List<Arista<String>> obtenerAristasEsperadasGrafoEjDiapos() {
		List<Arista<String>> aristasEsp = new ArrayList<Arista<String>>();
		
		aristasEsp.add(new Arista<String>("A", "B", 0.4));
		aristasEsp.add(new Arista<String>("A", "H", 0.8));
		aristasEsp.add(new Arista<String>("C", "I", 0.3));
		aristasEsp.add(new Arista<String>("C", "F", 0.4));
		aristasEsp.add(new Arista<String>("H", "G", 0.1));
		aristasEsp.add(new Arista<String>("G", "F", 0.3));
		aristasEsp.add(new Arista<String>("C", "D", 0.6));
		aristasEsp.add(new Arista<String>("D", "E", 0.9));
		
		return aristasEsp;
	}
	
	public static Grafo<String> obtenerGrafoPocosVertices() {
		Grafo<String> grafo = new Grafo<String>();
		grafo.agregarAristaYPeso("A", "B", 0.2);
		grafo.agregarAristaYPeso("A", "C", 0.5);
		grafo.agregarAristaYPeso("A", "E", 0.6);
		grafo.agregarAristaYPeso("B", "E", 0.2);
		grafo.agregarAristaYPeso("B", "D", 0.3);
		grafo.agregarAristaYPeso("B", "C", 0.5);
		grafo.agregarAristaYPeso("D", "E", 0.4);
		grafo.agregarAristaYPeso("C", "D", 0.4);
		
		return grafo;
	}
	
	public static Grafo<String> grafoDeStringConPesosRepetidos() {
		Grafo<String> grafo = new Grafo<String>();
		
		grafo.agregarAristaYPeso("Juan", "Pedro", 0.1);
		grafo.agregarAristaYPeso("Raul", "Pedro", 0.1);
		grafo.agregarAristaYPeso("Luis", "Pedro", 0.1);
		grafo.agregarAristaYPeso("Raul",  "Luis", 0.2);
		grafo.agregarAristaYPeso("Jose", "Luis", 0.2);
		
		return grafo;
	}
	
	
	//GRAFO<INTEGER>
	public static Grafo<Integer> grafoDeIntegerConPesosRepetidos() {
		Grafo<Integer> grafo = new Grafo<Integer>();
			
		grafo.agregarAristaYPeso(1, 2, 0.1);
		grafo.agregarAristaYPeso(2, 3, 0.3);
		grafo.agregarAristaYPeso(3, 4, 0.5);
		grafo.agregarAristaYPeso(2, 4, 0.3);
		grafo.agregarAristaYPeso(5, 4, 0.4);
		grafo.agregarAristaYPeso(1, 3, 0.6);
		grafo.agregarAristaYPeso(2, 5, 0.2);
			
		return grafo;
	}
	
	public static Grafo<Integer> AGMdeGrafoConPesosRepetidos() {
		Grafo<Integer> grafo = new Grafo<Integer>();
		
		grafo.agregarAristaYPeso(1, 2, 0.1);
		grafo.agregarAristaYPeso(2, 3, 0.3);
		grafo.agregarAristaYPeso(2, 4, 0.3);
		grafo.agregarAristaYPeso(2, 5, 0.2);
		
		return grafo;
	}
	
	public static Grafo<Integer> grafoConPesosDistintos() {
		Grafo<Integer> grafo = new Grafo<Integer>();
		
		grafo.agregarAristaYPeso(1, 2, 0.1);
		grafo.agregarAristaYPeso(2, 3, 0.6);
		grafo.agregarAristaYPeso(3, 4, 0.4);
		grafo.agregarAristaYPeso(2, 4, 0.5);
		grafo.agregarAristaYPeso(5, 4, 0.3);
		grafo.agregarAristaYPeso(1, 3, 0.7);
		grafo.agregarAristaYPeso(2, 5, 0.2);
		
		return grafo;
	}
	
	public static Grafo<Integer> AGMGrafoConPesosDistintos(){
		Grafo<Integer> grafo = new Grafo<Integer>();
		
		grafo.agregarAristaYPeso(1, 2, 0.1);
		grafo.agregarAristaYPeso(3, 4, 0.4);
		grafo.agregarAristaYPeso(5, 4, 0.3);
		grafo.agregarAristaYPeso(2, 5, 0.2);
		
		return grafo;
	}
	
	//OPERATIVO
	/* COMUNICACION ESPIAS
	 
	Juan----Pedro      Jose
	        /    \     / 
	       /      \   /
	     Raul----- Luis
	
	
   AGM
   
   Juan----Pedro      Jose
	        /   \      / 
	       /     \    /
	     Raul     Luis
   
*/	
	public static Operativo operativoConProbabilidadesRepetidas() {
		Operativo operativo = new Operativo();

		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Pedro");
		Espia e3 = new Espia("Jose");
		Espia e4 = new Espia("Raul");
		Espia e5 = new Espia("Luis");
		
		operativo.agregarEspia(e1);
		operativo.agregarEspia(e2);
		operativo.agregarEspia(e3);
		operativo.agregarEspia(e4);
		operativo.agregarEspia(e5);
		
		operativo.agregarPosibilidadEncuentro(e1, e2, 0.1);
		operativo.agregarPosibilidadEncuentro(e4, e2, 0.1);
		operativo.agregarPosibilidadEncuentro(e5, e2, 0.1);
		operativo.agregarPosibilidadEncuentro(e4, e5, 0.2);
		operativo.agregarPosibilidadEncuentro(e3, e5, 0.2);
		
		return operativo;
	}
	
/* COMUNICACION ESPIAS
 
	Juan----Pedro      Jose
	        /    \     / 
	       /      \   /
	     Raul----- Luis
	
	
   AGM
   
   Juan----Pedro      Jose
	        /          / 
	       /          /
	     Raul----- Luis
   
*/
	
	public static Operativo operativoConProbabilidadesDiferentes() {
		Operativo operativo = new Operativo();

		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Pedro");
		Espia e3 = new Espia("Jose");
		Espia e4 = new Espia("Raul");
		Espia e5 = new Espia("Luis");
		
		operativo.agregarEspia(e1);
		operativo.agregarEspia(e2);
		operativo.agregarEspia(e3);
		operativo.agregarEspia(e4);
		operativo.agregarEspia(e5);
		
		operativo.agregarPosibilidadEncuentro(e1, e2, 0.3);
		operativo.agregarPosibilidadEncuentro(e4, e2, 0.4);
		operativo.agregarPosibilidadEncuentro(e5, e2, 0.6);
		operativo.agregarPosibilidadEncuentro(e4, e5, 0.5);
		operativo.agregarPosibilidadEncuentro(e3, e5, 0.7);
		
		return operativo;
	}
	
	public static Operativo operativoGrafoNoConexo() {
		Operativo operativo = new Operativo();

		Espia e1 = new Espia("Juan");
		Espia e2 = new Espia("Pedro");
		Espia e3 = new Espia("Jose");

		operativo.agregarEspia(e1);
		operativo.agregarEspia(e2);
		operativo.agregarEspia(e3);

		operativo.agregarPosibilidadEncuentro(e1, e2, 0.3);
		
		return operativo;

	}

	
	
}
