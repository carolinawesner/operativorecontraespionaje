package view;

import javax.swing.JFrame;
import controller.Datos;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Set;
import java.awt.event.ActionEvent;
import javax.swing.JSlider;
import javax.swing.UIManager;

import java.awt.Toolkit;

public class VentanaConectarEspias extends JDialog {

	private static final long serialVersionUID = 1L;
	private Datos datosOperativo;
	private boolean fueEnviado;							//True si se presiono el bot�n ENVIAR, False en caso contrario.
	private boolean seModificoProbabilidad;				//True si se modifico la probabilida de un encuentro existente, False en caso contrario.
	private String primerEspia;
	private String segundoEspia;
	private double probabilidad;
	private JSlider slider;
	private JFrame frame;

	/**
	 * Crea la ventana y carga los datos pasados por par�metro.
	 * @param frame: corresponde a la ventana de retorno.
	 * @param d: corresponde a los datos del operativo.
	 */
	public VentanaConectarEspias(JFrame frame, Datos d) {
		super(frame, true);
		
		try {
			UIManager.setLookAndFeel(
			UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) { 
			e.printStackTrace();		
		} 
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaConectarEspias.class.getResource("/imagenes/conectar.png")));
		setTitle("Establecer encuentro");
		this.frame = frame;
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.datosOperativo = d;
		fueEnviado = false;
		seModificoProbabilidad = false;
		initialize();
	}
	
	/**
	 * Devuelve la probabilidad de intercepci�n seleccionada por el usuario.
	 */
	public double obtenerProbabilidad() {
		return probabilidad;
	}
	
	/**
	 * Devuelve el primer esp�a seleccionado por el usuario.
	 */
	public String obtenerPrimerEspia() {
		return primerEspia;
	}
	
	/**
	 * Devuelve el segundo esp�a seleccionado por el usuario.
	 */
	public String obtenerSegundoEspia() {
		return segundoEspia;
	}

	/**
	 * Identifica si fue presionado el bot�n de enviar.
	 * @return true si se presion�, false en caso contrario.
	 */
	public boolean fueEnviado() {
		return fueEnviado;
	}
	
	/**
	 * Identifica si se modifico una probabilidad de intercepci�n
	 * ya registrada.
	 * @return true si se modific�, false en caso contrario.
	 */
	public boolean seModificoProbabilidadExistente() {
		return seModificoProbabilidad;
	}
	
	
	/**
	 * Inicializa los componentes de la ventana.
	 */
	private void initialize() {
		
		//CONFIGURACI�M
		getContentPane().setLayout(null);
		setBounds(295, 240, 310, 220);
		
		//COMBO BOX: PRIMER ESP�A
		JComboBox<String> comboBoxEspia1 = crearComboBoxConEspias(24);
		
		//COMBO BOX: ESP�A CON EL QUE SE COMUNICAR� EL PRIMER ESP�A
		JComboBox<String> comboBoxEspia2 = crearComboBoxConEspias(68);
		
		//MUESTRA EL SLIDER POR EL QUE SE ELIGE LA PROBABILIDAD
		mostrarSlider();
		
		//BOTON ENVIAR
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
	
				try {
					primerEspia = (String)comboBoxEspia1.getSelectedItem();
					segundoEspia = (String)comboBoxEspia2.getSelectedItem();
					probabilidad = slider.getValue() / 10.0;
					
					if(datosOperativo.conexionRegistrada(segundoEspia, primerEspia)) {
						int opcion = JOptionPane.showConfirmDialog(frame, "Ya existe esta conexi�n, �desea modificarla?", 
								"MODIFICAR PROBABILIDAD DE INTERCEPCION", 0, 3);
						if(opcion == 0) {
							datosOperativo.modificarProbabilidadDeIntercepcion(primerEspia, segundoEspia, probabilidad);
							fueEnviado = true;
							seModificoProbabilidad = true;
						}
					} else {
						datosOperativo.agregarConexion(primerEspia, segundoEspia, probabilidad);
						fueEnviado = true;
					}
				}
				catch(Exception ex) {
					JOptionPane.showMessageDialog(frame, ex.getMessage(),"ERROR",0);
				}
				setVisible(false);
			}
		});
		btnEnviar.setBounds(157, 143, 101, 23);
		getContentPane().add(btnEnviar);
	
		//CREA ETIQUETAS DE INFORMACI�N
		crearEtiquetas();
		
		//BOTON VOLVER
		botonVolver();
	}

	/**
	 * Devuelve un combo box con todos los esp�as del operativo.
	 * @param y: corresponde al valor y de la posici�n del combo box.
	 */
	private JComboBox<String> crearComboBoxConEspias(int y) {
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		comboBox.setBounds(101, y, 154, 18);
		cargarEspiasComboBox(comboBox, datosOperativo.darNombresEspias());
		getContentPane().add(comboBox);
		
		return comboBox;
	}

	/**
	 * Guarda los esp�as de nombres en el combo box comboBox.
	 */
	private void cargarEspiasComboBox (JComboBox<String> comboBox, Set<String> nombres) {
		for (String nombre : nombres ) {
			comboBox.addItem(nombre);
		}
	}
	
	/**
	 * Muestra un slider en la pantalla, con valores entre 0 y 1.
	 */
	private void mostrarSlider() {
		JLabel valorSlider = new JLabel("");
		valorSlider.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		valorSlider.setBounds(262, 109, 32, 23);
		getContentPane().add(valorSlider);
		
		slider = new JSlider(0, 10);
		slider.setBounds(101, 114, 154, 18);
		slider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent event) {
            	double valor = (double) slider.getValue() / 10;
            	valorSlider.setText(String.valueOf(valor));
            }
		});
		getContentPane().add(slider);
	}
	
	/**
	 * Crea el bot�n para cancelar la operaci�n.
	 */
	private void botonVolver() {
		JButton btnVolver = new JButton("Volver");
		btnVolver.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		btnVolver.setBounds(29, 143, 101, 23);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fueEnviado = false;
				setVisible(false);
			}
		});
		getContentPane().add(btnVolver);
	}
	
	/**
	 * Crea las etiquetas correspondiente al primer esp�a, segundo esp�a,
	 * y probabilidad de intercepci�n.
	 */
	private void crearEtiquetas() {
		//ETIQUETA PARA ELEGIR PRIMER ESP�A
		JLabel lblEspia1 = new JLabel("Primer esp\u00EDa");
		lblEspia1.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		lblEspia1.setBounds(10, 11, 101, 14);
		getContentPane().add(lblEspia1);
		
		//ETIQUETA PARA ELEGIR SEGUNDO ESP�A
		JLabel lblEspia2 = new JLabel("Segundo esp\u00EDa");
		lblEspia2.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		lblEspia2.setBounds(10, 44, 114, 21);
		getContentPane().add(lblEspia2);
		
		//ETIQUETA PARA ELEGIR PROBABILIDAD DE INTERCEPCI�N
		JLabel lblProbabilidadIntercepcion = new JLabel("Probabilidad de intercepci\u00F3n");
		lblProbabilidadIntercepcion.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		lblProbabilidadIntercepcion.setBounds(10, 86, 202, 26);
		getContentPane().add(lblProbabilidadIntercepcion);
	}
}
