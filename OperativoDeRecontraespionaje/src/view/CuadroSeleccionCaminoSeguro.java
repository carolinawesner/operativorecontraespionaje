package view;


import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapObjectImpl;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import controller.Datos;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import java.awt.Toolkit;

public class CuadroSeleccionCaminoSeguro extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private Datos datosOperativo;
	private Map<String, MapMarkerDot> nombresYpuntos;
	private List<String[]> listaNombres;
	private String algoritmoElegido;
	private Set<MapPolygon> listaPoligono; 		//lista de poligonos del camino seguro
	

	/**
	 * Crea un cuadro con todos los componentes para que el usuario pueda
	 * elegir el algorimo deseado para calcular el camino seguro en un
	 * conjunto de esp�as de un mapa.
	 * @param frame: corresponde al frame al que se debe retornar.
	 * @param d: corresponde a los datos de los esp�as registrados.
	 * @param map: corresponde a los puntos en el mapa de los esp�as.
	 */
	public CuadroSeleccionCaminoSeguro(JFrame frame, Datos d, Map<String, MapMarkerDot> map) {
		configurarCuadro();

		this.datosOperativo = d;
		this.nombresYpuntos = map;
	
		mostrarTitulo();
		
		//COMBO BOX: ELECCI�N DE ALGORITMO
		JComboBox<String> comboBoxAlgoritmo = new JComboBox<String>();
		comboBoxAlgoritmo.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		comboBoxAlgoritmo.setModel(new DefaultComboBoxModel<String>(new String[] {"Kruskal", "Prim"}));
		comboBoxAlgoritmo.setBounds(90, 36, 180, 20);
		getContentPane().add(comboBoxAlgoritmo);
		
		//BOT�N PARA MOSTRAR EL CAMINO SEGURO EN EL MAPA
		JButton btnMapa = new JButton("Ver en el mapa");
		btnMapa.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		btnMapa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					if(!mostrarVentanasDatosVaciosONulos(frame)) {
						algoritmoElegido = obtenerAlgoritmo(comboBoxAlgoritmo);
						listaNombres = datosOperativo.listaDeDuplasNombres(algoritmoElegido);
						armarPoligono(listaNombres);
						setVisible(false);	
					}
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(frame, ex.getMessage(),"ERROR",0);
				}
				
			}
		});
		btnMapa.setBounds(10, 67, 154, 20);
		getContentPane().add(btnMapa);
		
		//BOT�N PARA MOSTRAR EL CAMINO DE FORMA ESCRITA
		JButton btnMostrarCamino = new JButton("Leer camino seguro");
		btnMostrarCamino.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		btnMostrarCamino.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if(!mostrarVentanasDatosVaciosONulos(frame)) {
						algoritmoElegido = obtenerAlgoritmo(comboBoxAlgoritmo);
						listaNombres = datosOperativo.listaDeDuplasNombres(algoritmoElegido);
		
						CuadroDialogo cuadroTexto = new CuadroDialogo(frame, "Comunicaci�n a realizar",
								darStringCaminoElegido(algoritmoElegido, datosOperativo));
						
						cuadroTexto.setVisible(true);
						setVisible(false);
					}
					
				}
				catch (Exception ex) {
					JOptionPane.showMessageDialog(frame, ex.getMessage(),"ERROR",0);
				}
			}
		});
		btnMostrarCamino.setBounds(174, 67, 154, 20);
		getContentPane().add(btnMostrarCamino);
	
	}
	
	private void configurarCuadro() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(CuadroSeleccionCaminoSeguro.class.getResource("/imagenes/conectar.png")));
		setTitle("Opciones para el mejor camino");
		setModal(true);
		setBounds(270, 260, 380, 140);
		getContentPane().setLayout(null);
		
		try {
			UIManager.setLookAndFeel(
			UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) { 
			e.printStackTrace();		
		} 
	}
	
	private void mostrarTitulo() {
		JLabel lblEleccionAlgoritmo = new JLabel("Elija el algoritmo para establecer el camino mas seguro:");
		lblEleccionAlgoritmo.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		lblEleccionAlgoritmo.setBounds(10, 11, 414, 22);
		getContentPane().add(lblEleccionAlgoritmo);
	}

	/**
	 * Obtiene la comunicaci�n segura en forma de String.
	 * @param algoritmo corresponde al algoritmo seleccionado por el usuario.
	 * @param d corresponde a los datos de los esp�as.
	 * @return String correspondiente al camino seguro obtenido.
	 */
	private String darStringCaminoElegido(String algoritmo, Datos d) {
		if(algoritmo.equals("Prim")) {
			return d.comunicacionPrim();
		}
		else {
			return d.comunicacionKruskal();
		}
	}
	
	/**
	 * Devuelve la selecci�n del combobox que corresponde al nombre de un algoritmo.
	 */
	private String obtenerAlgoritmo (JComboBox<String> comboBox) {
		return (String)comboBox.getSelectedItem();	
	}
	
	/**
	 * Guarda los pol�gonos correspondientes al camino seguro
	 * seg�n la lista pasada por par�metro.
	 */
	private void armarPoligono(List<String[]> listaNombres) {
		listaPoligono = new HashSet<MapPolygon>();
		for (String[] a : listaNombres) {
			ArrayList<Coordinate> coordenadas = new ArrayList<Coordinate>();
			Coordinate coord1 = nombresYpuntos.get(a[0]).getCoordinate();
			Coordinate coord2 = nombresYpuntos.get(a[1]).getCoordinate();
			
			coordenadas.add(coord1);
			coordenadas.add(coord2);
			coordenadas.add(coord2);
			
			MapPolygon poligono = new MapPolygonImpl(coordenadas); 
			((MapObjectImpl) poligono).setColor(Color.green);
			listaPoligono.add(poligono);
		}
	}
	
	/**
	 * Devuelve los pol�gonos correspondientes a la comunicaci�n segura
	 * seg�n el algoritmo seleccionado.
	 */
	public Set<MapPolygon> darPolComSegura() {
		return this.listaPoligono;
	}
	
	/**
	 * Muestra un mensaje de error si los datos ingresados son vac�os o nulos. 
	 * @return true si los datos ingresados son vac�os o nulos, y false
	 * en caso contrario.
	 */
	private boolean mostrarVentanasDatosVaciosONulos(JFrame frame) {
		if(datosOperativo == null || datosOperativo.isEmpty()) {
			JOptionPane.showMessageDialog(frame, "Debe haber al menos una conexi�n registrada", "ERROR", 0);
			return true;
		}
		return false;
	}
}
