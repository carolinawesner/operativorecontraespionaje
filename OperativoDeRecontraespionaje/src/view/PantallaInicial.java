package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import controller.Datos;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import java.awt.Color;

public class PantallaInicial {

	private JFrame pantallaInicial;
	private Fondo fondo = new Fondo("/imagenes/fondo.jpg");
	private Datos obtencionDatosOperativo;

	/**
	 * Muestra u oculta este objeto.
	 * @param b corresponde a un boolean tal que si b == true
	 * esta pantalla se muestra, y si b == false esta pantalla se oculta.
	 */
	public void setVisible(boolean b) {
		pantallaInicial.setVisible(b);
	}	
	
	/**
	 *	Ejecuta la aplicaci�n.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaInicial window = new PantallaInicial();
					window.pantallaInicial.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Crea la aplicaci�n.
	 */
	public PantallaInicial() {
		try {
			UIManager.setLookAndFeel(
			UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) { 
			e.printStackTrace();		
		} 
		
		initialize();
	}

	/**
	 * Inicializa los contenidos de la ventana.
	 */
	private void initialize() {
		pantallaInicial = new JFrame();	
		pantallaInicial.setIconImage(Toolkit.getDefaultToolkit().getImage(PantallaInicial.class.getResource
				("/imagenes/icono.png")));
		pantallaInicial.setTitle("Temible Operativo De Recontraespionaje");
		pantallaInicial.setContentPane(fondo);
		pantallaInicial.setResizable(false);
		pantallaInicial.setBounds(100, 100, 725, 506);
		pantallaInicial.setBackground(Color.black);
		pantallaInicial.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pantallaInicial.getContentPane().setLayout(null);
		
		iniciarOperativo();
		recuperarUltimoOperativo();
		HerramientasInterfaz.crearBarraHerramientas(pantallaInicial);
	}
	
	/**
	 * Crea un bot�n que inicia el operativo.
	 */
	private void iniciarOperativo() {
		JButton btnIniciar = HerramientasInterfaz.crearBotonPantallaInicial("INICIAR NUEVO OPERATIVO");
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				obtencionDatosOperativo = new Datos();
				MapaDeOperativo mapa = new MapaDeOperativo(obtencionDatosOperativo);
				mapa.setVisible(true);
				pantallaInicial.setVisible(false);
			}
		});
		btnIniciar.setBounds(124, 270, 261, 39);
		pantallaInicial.getContentPane().add(btnIniciar);
	}
	
	/**
	 * Crea un bot�n que recupera el �ltimo operativo guardado.
	 */
	private void recuperarUltimoOperativo() {
		JButton btnRecuperarOp = HerramientasInterfaz.crearBotonPantallaInicial("RECUPERAR ULTIMO OPERATIVO");
		btnRecuperarOp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					obtencionDatosOperativo = Datos.recuperarDatos();
					MapaDeOperativo mapa = new MapaDeOperativo(obtencionDatosOperativo);
					mapa.setVisible(true);
					pantallaInicial.setVisible(false);
					
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(pantallaInicial, 
							"Ocurri� un error. Int�ntelo mas tarde", "ERROR", 0);
				}
			}
		});
		btnRecuperarOp.setBounds(89, 320, 324, 39);
		pantallaInicial.getContentPane().add(btnRecuperarOp);
	}
	
}
