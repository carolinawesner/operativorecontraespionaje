package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;


public class HerramientasInterfaz {

	/**
	 * Crea un bot�n que no es utilizado en la pantalla inicial de la aplicaci�n.
	 * @param titulo: corresponde al texto que se mostrar� en el bot�n.
	 * @param x: corresponde al valor x de la coordenada del bot�n dentro de la ventana.
	 * @param y: corresponde al valor y de la coordenada del bot�n dentro de la ventana.
	 * @return JButton creado con estos datos, con tama�o, color y fuente predeterminada.
	 */
	public static JButton crearBotonSecundarioPredeterminado(String titulo, int x, int y) {
		JButton button = new JButton(titulo);
		button.setBounds(x, y, 208, 23);
		button.setBackground(new Color(255, 127, 80));
		button.setFont(new Font("Century Schoolbook", Font.PLAIN, 12));
		return button;
	}
	
	/**
	 * Crea un bot�n que es usado en la pantalla inicial de la aplicaci�n.
	 * @param titulo: corresponde al texto que se mostrar� en el bot�n.
	 * @return JButton creado con este dato, con fuente y color predeterminado.
	 */
	public static JButton crearBotonPantallaInicial(String titulo) {
		JButton button = new JButton(titulo);
		button.setForeground(new Color(0, 0, 0));
		button.setBackground(new Color(205, 133, 63));
		button.setFont(new Font("Century Schoolbook", Font.PLAIN, 15));
		return button;
	}
	
	/**
	 * Crea una barra de herramientas con preguntas frecuentes de los usuarios.
	 * @param pantallaInicial: corresponde a la pantalla donde se crea esta
	 * barra de herramientas.
	 */
	public static void crearBarraHerramientas(JFrame pantallaInicial) {
		JMenu preguntas = new JMenu("Preguntas frecuentes");
		String queEs = "�Qu� es un Operativo de Recontraespionaje?";
		String comoFunciona = "�C�mo funciona un Operativo de Recontraespionaje?";
		JMenuItem pregunta1 = new JMenuItem(queEs);

		pregunta1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CuadroDialogo pregQueEs = new CuadroDialogo(pantallaInicial, queEs,
						HerramientasInterfaz.respuestPreg1());
				tituloEiconoJDialog(pregQueEs);
				pregQueEs.setVisible(true);
			}
		});
		preguntas.add(pregunta1);
		
		JMenuItem pregunta2 = new JMenuItem(comoFunciona);
		pregunta2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CuadroDialogo pregComo = new CuadroDialogo(pantallaInicial, comoFunciona, 
						HerramientasInterfaz.respuestPreg2());
				tituloEiconoJDialog(pregComo);
				pregComo.setVisible(true);
			}
		});
		preguntas.add(pregunta2);
		
		JMenuBar barraDeMenu = new JMenuBar();
		barraDeMenu.add(preguntas);
		pantallaInicial.setJMenuBar(barraDeMenu);
	}
	
	/**
	 * Setea �cono y t�tulo predeterminado para preguntas frecuentes
	 * al cuadro pasado por par�metro.
	 */
	private static void tituloEiconoJDialog(JDialog cuadro) {
		cuadro.setIconImage(Toolkit.getDefaultToolkit().getImage(CuadroDialogo.class.getResource("/imagenes/pregunta.png")));
		cuadro.setTitle("Preguntas Frecuentes");
	}
	
	/**
	 * Devuelve la respuesta a la pregunta �Qu� es un Operativo de Recontraespionaje?.
	 */
	private static String respuestPreg1() {
		String resp = "Es una aplicaci�n para simular un operativo formado por esp�as "
				+ "que se encuentran en el territorio enemigo y necesitan comunicarse.\n"
				+ "Y, de esta manera, lograr que un mensaje llegue a todos ellos con la "
				+ "menor posibilidad de que el enemigo intercepte sus encuentros.\n"
				+ "La aplicaci�n permite mostrar el camino m�s seguro entre esp�as con "
				+ "posibilidad de encuentro.\n";
		return resp;
	}

	/**
	 * Devuelve la respuesta a la pregunta �C�mo funciona un Operativo de Recontraespionaje?
	 */
	private static String respuestPreg2() {
		String resp = "La aplicaci�n permite: \n"
				+ "-	Agregar esp�as en el mapa, para esto se debe apuntar en la ubicaci�n deseada "
				+ "e ingresar un nombre. Este no puede repetirse.\n"
				+ "-	Eliminar esp�as registrados apretando �Eliminar espia� y eligiendo el esp�a deseado.\n"
				+ "-	Establecer un encuentro entre esp�as registrados apretando �Establecer Encuentro� "
				+ "y eligiendo las opciones deseadas.\n"
				+ "-	Eliminar un encuentro registrado apretando �Eliminar encuentro� y eligiendo los "
				+ "esp�as correspondientes.\n"
				+ "-	Obtener el camino m�s seguro entre los esp�as con posibilidad de encuentro apretando "
				+ "�Camino m�s seguro�. En este caso, se podr� elegir el algoritmo a utilizar y si se desea "
				+ "leer el camino o verlo en el mapa.\n"
				+ "-	Guardar y recuperar el �ltimo registro del operativo.\n";
		return resp;
	}
	
	/**
	 * Devuelve la fuente predeterminada para cuadros de di�logo.
	 */
	public static Font fuenteCuadrosDeDialogo() {
		return new Font("Microsoft YaHei UI", Font.PLAIN, 12);
	}
}
