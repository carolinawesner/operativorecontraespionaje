package view;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.Toolkit;

public class CuadroDialogo extends JDialog {

	private static final long serialVersionUID = 1L;
	private Fondo fondo = new Fondo("/imagenes/fondoDegradado.jpg");


	/**
	 * Crea el cuadro de di�logo.
	 */
	public CuadroDialogo(JFrame frameRetorno, String titulo, String texto) {
		configurarCuadro();
		mostrarTexto(texto);
		mostrarTitulo(titulo);
		crearBotonOk(frameRetorno);
	}
	
	/**
	 * Configura la ventana.
	 */
	private void configurarCuadro() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(CuadroDialogo.class.getResource("/imagenes/pregunta.png")));
		setAlwaysOnTop(true);
		setModal(true);
		setBounds(100, 100, 725, 506);
		this.setContentPane(fondo);
		fondo.setLayout(null);
		
		try {
			UIManager.setLookAndFeel(
			UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) { 
			e.printStackTrace();		
		} 
		
	}
	
	/**
	 * Muestra el texto en la ventana.
	 */
	private void mostrarTexto(String texto) {
		JTextPane textPane = new JTextPane();
		textPane.setFont(new Font("Century Schoolbook", Font.PLAIN, 20));
		textPane.setEditable(false);
		textPane.setOpaque(true);
		textPane.setBackground(new Color(222, 184, 135));
		textPane.setBounds(1, 1, 653, 334);
		getContentPane().add(textPane);
		textPane.setText(texto);
		
		JScrollPane scrollPane = new JScrollPane(textPane);
		scrollPane.setBounds(10, 69, 672, 336);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setOpaque(false);
		scrollPane.getViewport().setOpaque(false);
		super.getContentPane().add(scrollPane);
	}
	
	/**
	 * Crea un bot�n Ok, que si es presionado se va al frame
	 * pasado por par�metro.
	 * @param frameRetorno corresponde al frame que se mostrar�.
	 */
	private void crearBotonOk(JFrame frameRetorno) {
		JPanel buttonPane = new JPanel();
		buttonPane.setBounds(0, 434, 709, 33);
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPane.setOpaque(false);
		getContentPane().add(buttonPane);
		
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frameRetorno.setVisible(true);
				setVisible(false);
			}
		});
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
	}
	
	/**
	 * Coloca el string tomado por par�metro como t�tulo de la ventana.
	 */
	private void mostrarTitulo(String titulo) {
		JLabel lblTitulo = new JLabel(titulo);
		lblTitulo.setFont(new Font("Script MT Bold", Font.PLAIN, 29));
		lblTitulo.setBounds(10, 15, 676, 53);
		getContentPane().add(lblTitulo);
	}
	
}
