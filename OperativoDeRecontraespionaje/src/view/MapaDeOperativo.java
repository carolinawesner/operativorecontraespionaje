package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapObjectImpl;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import controller.Datos;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class MapaDeOperativo {
	
	private JFrame frmTemibleOperativoDe;
	private Fondo fondo = new Fondo("/imagenes/fondoDegradado.jpg");
	private JPanel panelControles;
	private JMapViewer mapa;
	private Set<MapPolygon> poligonos;
	private Datos datosOperativo;
	private Map<String, MapMarkerDot> nombresYpuntos;
	private Set<MapPolygon> PolCaminoSeguro;
	
	/**
	 * Muestra u oculta este objeto.
	 * @param b corresponde a un boolean tal que si b == true
	 * esta pantalla se muestra, y si b == false esta pantalla se oculta.
	 */
	public void setVisible(boolean b) {
		frmTemibleOperativoDe.setVisible(b);
	}	


	/**
	 * Crea una ventana que contiene un mapa en el que se mostrar� 
	 * toda la informaci�n correspondiente a los esp�as.
	 * @param d: corresponde a los datos donde se almacenar� la
	 * informaci�n ingresada por el usuario, o, tambi�n, puede ser
	 * correspondiente a datos de un operativo anterior.
	 */
	public MapaDeOperativo(Datos d) {
		datosOperativo = d;
		nombresYpuntos = new HashMap<String, MapMarkerDot>();	
		poligonos = new HashSet<MapPolygon>();
		
		try {
			UIManager.setLookAndFeel(
			UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) { 
			e.printStackTrace();		
		} 
		
		initialize();
	}

	/**
	 * Inicializa todos los componentes de esta ventana.
	 */
	private void initialize() {
	
		//FRAME
		frmTemibleOperativoDe = new JFrame();
		frmTemibleOperativoDe.setIconImage(Toolkit.getDefaultToolkit().getImage(MapaDeOperativo.class.getResource("/imagenes/icono.png")));
		frmTemibleOperativoDe.setTitle("Temible Operativo De Recontraespionaje");
		frmTemibleOperativoDe.setBounds(100, 100, 725, 506);
		frmTemibleOperativoDe.setContentPane(fondo);
		frmTemibleOperativoDe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTemibleOperativoDe.getContentPane().setLayout(null);
		
		//MAPA Y PANEL DE MAPA
		JPanel panelMapa = new JPanel();
		panelMapa.setBounds(10, 11, 689, 365);
		frmTemibleOperativoDe.getContentPane().add(panelMapa);
		mapa = new JMapViewer();
		mapa.setLocation(0, 0);
		mapa.setScrollWrapEnabled(true);
		mapa.setDisplayPosition(new Coordinate(-34.521, -58.7008), 15);
		panelMapa.setLayout(null);
		mapa.setSize(689, 365);
		mapa.setMinimumSize(new Dimension(689, 410));
		panelMapa.add(mapa);
		
		
		//CONTROLES
		panelControles = new JPanel();
		panelControles.setBounds(10, 354, 689, 102);
		panelControles.setOpaque(false);
		panelControles.setLayout(null);
		frmTemibleOperativoDe.getContentPane().add(panelControles);
		
		//BOTON ELIMINAR ESPIA
		btnEliminarEspias();
		
		//BOTON COMUNICACION SEGURA
		btnComunicacionSegura();
		
		//BOTON GUARDAR DATOS
		btnGuardarDatos();
		
		//BOTON ELIMINAR ENCUENTRO
		btnEliminarEncuentro();
		
		//BOTON ESTABLECER ENCUENTRO
		btnEstablecerEncuentro();
		
		//BOTON VOLVER
		btnVolver();

		if(datosOperativo != null && !datosOperativo.isEmpty())
			recuperarDatos();

		//ALMACENAMIENTO DE ESP�AS 
		almacenarEspia();
	}
	
	/**
	 * Crea un bot�n que elimina del mapa un esp�a seleccionado.
	 */
	private void btnEliminarEspias() {
		JButton btnEliminarEspia = HerramientasInterfaz.crearBotonSecundarioPredeterminado
				("ELIMINAR ESPIA", 90, 45);
		panelControles.add(btnEliminarEspia);
		btnEliminarEspia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object[] nombresDeEspias = datosOperativo.darNombresEspias().toArray();

				String nombre = (String) JOptionPane.showInputDialog(frmTemibleOperativoDe, "Esp�a a eliminar:", 
						"ELIMINAR ESP�A", 0, null, nombresDeEspias, null);
				if(nombre != null) {
					eliminarPoligonosDe(nombre);
					mapa.removeMapMarker(nombresYpuntos.get(nombre));
					nombresYpuntos.remove(nombre);
					datosOperativo.eliminarEspiaRegistrado(nombre);
					//elimina camino seguro si existe
					eliminarPolCaminoSeguro();
				}		
			}
		});
	}
	
	/**
	 * Crea un bot�n que calcula la comunicaci�n segura de los esp�as en el operativo.
	 * Si no hay esp�as o hay pero no est�n conectados, se mostrar� un cuadro de error.
	 */
	private void btnComunicacionSegura() {
		JButton btnComSegura = HerramientasInterfaz.crearBotonSecundarioPredeterminado
				("CAMINO MAS SEGURO", 306, 79);
		btnComSegura.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//elimina el camino anterior
				eliminarPolCaminoSeguro();
				CuadroSeleccionCaminoSeguro caminoSeguro = new CuadroSeleccionCaminoSeguro
						(frmTemibleOperativoDe, datosOperativo, nombresYpuntos);
				caminoSeguro.setVisible(true);
				PolCaminoSeguro = caminoSeguro.darPolComSegura();
				//muestra el nuevo camino
				if(PolCaminoSeguro != null)
					for(MapPolygon p : PolCaminoSeguro) {
						mapa.addMapPolygon(p);
				}
				;	
			}
		});
		panelControles.add(btnComSegura);
	}
	
	/**
	 * Crea un bot�n que elimina un encuentro establecido entre dos esp�as.
	 */
	private void btnEliminarEncuentro() {
		JButton btnEliminarEncuentro = HerramientasInterfaz.crearBotonSecundarioPredeterminado
				("ELIMINAR ENCUENTRO", 90, 79);
		btnEliminarEncuentro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VentanaEliminarEncuentroEspias seleccionar = 
						new VentanaEliminarEncuentroEspias(frmTemibleOperativoDe, datosOperativo);
				seleccionar.setTitle("SELECCIONA RELACION A BORRAR");
				seleccionar.setVisible(true);
				
				if(seleccionar.fueEnviado()) {
					String espia1 = seleccionar.obtenerPrimerEspia();
					String espia2 = seleccionar.obtenerSegundoEspia();
					eliminarPoligono(espia1, espia2);
					//elimina camino seguro si existe
					eliminarPolCaminoSeguro();
				}
			}
		});
		panelControles.add(btnEliminarEncuentro);
	}

	/**
	 * Crea un bot�n que establece un encuentro entre dos esp�as.
	 */
	private void btnEstablecerEncuentro() {
		JButton btnEstablecerEncuentro = HerramientasInterfaz.crearBotonSecundarioPredeterminado
				("ESTABLECER ENCUENTRO", 306, 45);
		panelControles.add(btnEstablecerEncuentro);
		btnEstablecerEncuentro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(nombresYpuntos.size() < 2)
					JOptionPane.showMessageDialog(frmTemibleOperativoDe, 
							"Debe haber un m�nimo de dos esp�as registrados para establecer "
							+ "una posibilidad de encuentro.", "ERROR", 0);
				else {
					VentanaConectarEspias conectar = new VentanaConectarEspias(frmTemibleOperativoDe, datosOperativo);
					conectar.setVisible(true);
					
					if(conectar.fueEnviado()) {
						String espia1 = conectar.obtenerPrimerEspia();
						String espia2 = conectar.obtenerSegundoEspia();
						
						if(conectar.seModificoProbabilidadExistente())
							eliminarPoligono(espia1, espia2);
						
						dibujarEncuentro(espia1, espia2);
						eliminarPolCaminoSeguro();
					}
				}
			}
		});
	}
	
	/**
	 * Crea un bot�n que guarda los datos de este operativo en un archivo predeterminado.
	 */
	private void btnGuardarDatos() {
		JButton btnGuardarDatos = new JButton();
		btnGuardarDatos.setIcon(new ImageIcon(MapaDeOperativo.class.getResource("/imagenes/botonGuardar.png")));
		btnGuardarDatos.setOpaque(false);
		btnGuardarDatos.setContentAreaFilled(false);
		btnGuardarDatos.setBorderPainted(false);
		btnGuardarDatos.setBounds(547, 35, 79, 71);
		btnGuardarDatos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int eleccion = JOptionPane.showConfirmDialog(frmTemibleOperativoDe, 
						"Si guarda este operativo se eliminar� el anterior, �Desea continuar?", 
						"�Desea guardar el operativo?", 0, 2);
				if(eleccion == 0) {
					boolean seAlmaceno = datosOperativo.guardarDatos();
					
					if(!seAlmaceno)
						JOptionPane.showMessageDialog(frmTemibleOperativoDe, 
								"Ocurri� un error. Int�ntelo mas tarde", "ERROR", 0);
					else 
						JOptionPane.showMessageDialog(frmTemibleOperativoDe, 
								""
								+ "El operativo se guard� correctamente", "GUARDADO", 1);
				}	
			}
		});
		panelControles.add(btnGuardarDatos);
	}
	
	/**
	 * Crea un bot�n que vuelve a la pantalla principal.
	 */
	private void btnVolver() {
		JButton btnVolver = new JButton();
		btnVolver.setIcon(new ImageIcon(MapaDeOperativo.class.getResource("/imagenes/botonVolver.png")));
		btnVolver.setOpaque(false);
		btnVolver.setContentAreaFilled(false);
		btnVolver.setBorderPainted(false);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PantallaInicial inicio = new PantallaInicial();
				inicio.setVisible(true);
				frmTemibleOperativoDe.setVisible(false);
			}
		});
		btnVolver.setBounds(590, 11, 89, 23);
		mapa.add(btnVolver);	
	}
	
	/**
	 * Crea un bot�n que recupera los datos de un operativo.
	 * Esto sucede cuando se llama al constructor con un 
	 * objeto Datos que no est� vac�o. Los esp�as y las relaciones
	 * se mostrar�n en el mapa.
	 */
	private void recuperarDatos() {

		for(String nombreEspia : datosOperativo.darNombresEspias()) {
			recuperarEspia(nombreEspia);
			
			for(String nombreEspiaVecino : datosOperativo.darVecinosDe(nombreEspia)) {
				recuperarEspia(nombreEspiaVecino);
				dibujarEncuentro(nombreEspia, nombreEspiaVecino);
			}
		}
	}

	/**
	 * Recupera de datosOperativo los datos correspondientes al esp�a 
	 * pasado por par�metro, y lo dibuja en el mapa si no est� almacenado
	 * en el map nombresYpuntos
	 */
	private void recuperarEspia(String nombre) {
		
		//Crea el punto si el espia no esta almacenado, es decir, no se recupero,
		//y lo almacena y muestra en el mapa.
		if(!nombresYpuntos.containsKey(nombre)) {
			double[] coordEspia = datosOperativo.obtenerCoordenadasEspia(nombre);
			Coordinate c = new Coordinate(coordEspia[1], coordEspia[0]);
			
			MapMarkerDot puntoEspia = new MapMarkerDot(nombre, c);
			mapa.addMapMarker(puntoEspia);
			nombresYpuntos.put(nombre, puntoEspia);
		}
	}
	
	/**
	 * Almacena un esp�a en el operativo.
	 * Esto sucede cuando el usuario hace click en el mapa e ingresa 
	 * un nombre para el esp�a.
	 */
	private void almacenarEspia() {
		mapa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			if (e.getButton() == MouseEvent.BUTTON1) {
				Coordinate markeradd = (Coordinate)
				mapa.getPosition(e.getPoint());	//obtiene las coordenadas del punto donde esta el mouse
				String nombre = JOptionPane.showInputDialog(frmTemibleOperativoDe,
						"Ingrese el nombre del esp�a: ", "ALMACENAR ESP�A", -1);//ingreso nombre

				//Si el usuario ingreso algo...
				if(!(nombre == null)) {
					if (!datosOperativo.esEspiaRegistrado(nombre)  && !nombre.isEmpty() ) {
						MapMarkerDot puntoVisible = new MapMarkerDot(nombre, markeradd);
						mapa.addMapMarker(puntoVisible);	//crea un punto con el nombre
						nombresYpuntos.put(nombre, puntoVisible);
						
						//agrega espia con coordenadas marcadas
						datosOperativo.agregarDatoEspia(nombre);
						double[] coord = {markeradd.getLon(), markeradd.getLat()};
						datosOperativo.guardarCoordenadasEspia(nombre, coord);
						eliminarPolCaminoSeguro();
									
					} else {
						JOptionPane.showMessageDialog(frmTemibleOperativoDe,
								"�El nombre est� en blanco o ya fue registrado!","ERROR", 0);
					}
				}
			}}
		});
	}

	/**
	 * Dibuja en el mapa un encuentro entre dos esp�as.
	 */
	private void dibujarEncuentro(String espia1, String espia2) {
		ArrayList<Coordinate> coordenadas = new ArrayList<Coordinate>();
		Coordinate coord1 = nombresYpuntos.get(espia1).getCoordinate();
		Coordinate coord2 = nombresYpuntos.get(espia2).getCoordinate();
		
		coordenadas.add(coord1);
		coordenadas.add(coord2);
		coordenadas.add(coord2);
		
		MapPolygon poligono = new MapPolygonImpl(coordenadas); 
		//cambiar color
		((MapObjectImpl) poligono).setColor(Color.red); 
		//agrega la probabilidad en el mapa
		((MapObjectImpl) poligono).setName((datosOperativo.darProbIntercepcion(espia1, espia2)).toString());
		mapa.addMapPolygon(poligono);
		poligonos.add(poligono);
	}
	
	/**
	 * Elimina todo los pol�gonos de un esp�a espec�fico.
	 * @param espia: corresponde al esp�a cuyos pol�gonos
	 * ser�n eliminados.
	 */
	private void eliminarPoligonosDe(String espia) {
		Coordinate c = nombresYpuntos.get(espia).getCoordinate();
		Iterator<MapPolygon> it = poligonos.iterator();
		
		while(it.hasNext()) {
			MapPolygon actual = it.next();
			if(actual.getPoints().contains(c)) {
				it.remove();
				mapa.removeMapPolygon(actual);
			}
		}
	}

	/**
	 * Elimina el pol�gono que tiene a espia1 y espia2 en sus extremos.
	 */
	private void eliminarPoligono(String espia1, String espia2) {
		Coordinate c1 = nombresYpuntos.get(espia1).getCoordinate();
		Coordinate c2 = nombresYpuntos.get(espia2).getCoordinate();
		Iterator<MapPolygon> it = poligonos.iterator();
		
		while(it.hasNext()) {
			MapPolygon actual = it.next();
			if(actual.getPoints().contains(c1) && actual.getPoints().contains(c2)) {
				it.remove();
				mapa.removeMapPolygon(actual);
			}
		}
	}
	
	/**
	 * Elimina el camino seguro del mapa.
	 */
	private void eliminarPolCaminoSeguro() {
		if (PolCaminoSeguro != null) {
			for(MapPolygon p : PolCaminoSeguro) {
				mapa.removeMapPolygon(p);
			}
		}
	}
}
	
		

