package view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;

import controller.Datos;

public class VentanaEliminarEncuentroEspias extends JDialog {

	private static final long serialVersionUID = 1L;
	private Datos datosOperativo;
	private boolean fueEnviado;
	private String primerEspia;
	private String segundoEspia;
	private JComboBox<String> comboBoxEspia1;
	private JComboBox<String> comboBoxEspia2;
	
	/**
	 * Crea el cuadro que permite eliminar un encuntro entre 
	 * dos esp�as seleccionados .
	 * @param frame 
	 */
	public VentanaEliminarEncuentroEspias(JFrame frame, Datos d) {
		super(frame, true);
		
		try {
			UIManager.setLookAndFeel(
			UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) { 
			e.printStackTrace();		
		} 
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		datosOperativo = d;
		fueEnviado = false;
		initialize();
	}
	
	/**
	 * Devuelve el primer espia seleccionado.
	 * @return String con el nombre del esp�a.
	 */
	public String obtenerPrimerEspia() {
		return primerEspia;
	}
	
	/**
	 * Devuelve el segundo espia seleccionado.
	 * @return String con el nombre del esp�a.
	 */
	public String obtenerSegundoEspia() {
		return segundoEspia;
	}

	/**
	 * Evalua si el usuario apret� el boton "enviar"
	 * @return
	 */
	public boolean fueEnviado() {
		return fueEnviado;
	}
	
	/**
	 * Inicializaci�n
	 */
	private void initialize() {
		
		getContentPane().setLayout(null);
		setBounds(320, 250, 265, 185);
		
		//combo box para elegir espia1
		comboBoxEspia1 = comboBoxPredeterminado(29);
		cargarEspiasComboBox(comboBoxEspia1, datosOperativo.darNombresEspias());
		
		//combo box para elegir espia2
		comboBoxEspia2 = comboBoxPredeterminado(74);
		
		//boton enviar
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		btnEnviar.setEnabled(false);
		btnEnviar.setBounds(147, 103, 89, 23);
		getContentPane().add(btnEnviar);
		
		//accion comboBox1 completa comboBox2 y habilita boton enviar
		comboBoxEspia1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

					String eleccion = comboBoxEspia1.getSelectedItem().toString();
					
					if(!eleccion.equals("-") && datosOperativo.darVecinosDe(eleccion).size() > 0) {
						Set<String> vecinos = datosOperativo.darVecinosDe(eleccion);
						cargarEspiasComboBox(comboBoxEspia2, vecinos);
						btnEnviar.setEnabled(true);
					}
					//si la opcion elegida no tiene vecinos
					if(datosOperativo.darVecinosDe(eleccion).size() == 0) {
						comboBoxEspia2.removeAllItems();
						comboBoxEspia2.addItem("-");
						btnEnviar.setEnabled(false);
					}
						
			}
		});
		
		//accion boton enviar (si est� habilitado)
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				//genera la conexion con todos los datos
		
				primerEspia = comboBoxEspia1.getSelectedItem().toString();
				segundoEspia = comboBoxEspia2.getSelectedItem().toString();
				datosOperativo.eliminarConexion(primerEspia, segundoEspia);
				fueEnviado = true;
				setVisible(false);	
			}
		});
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 12));
		btnVolver.setBounds(21, 103, 89, 23);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fueEnviado = false;
				setVisible(false);
			}
		});
		getContentPane().add(btnVolver);
		
		crearEtiquetasEspias();
	}

	/**
	 * Carga el conjunto de nombres al comboBox que recibe por par�metro.
	 * Si el conjunto no tiene elementos no hace nada.
	 * @param comboBox a cargar
	 * @param nombres conjunto de String con los nombres de los espias
	 */
	private void cargarEspiasComboBox (JComboBox<String> comboBox, Set<String> nombres) {
		if (nombres.size() > 0) {
			comboBox.removeAllItems();
			for (String nombre : nombres ) {
				comboBox.addItem(nombre);
			}
		}
	}
	
	/**
	 * Crea las etiquetas "Primer espia" y "Segundo espia"
	 */
	private void crearEtiquetasEspias() {
		JLabel lblEspia1 = new JLabel("Primer esp\u00EDa");
		lblEspia1.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		lblEspia1.setBounds(10, 11, 101, 14);
		getContentPane().add(lblEspia1);
		
		JLabel lblEspia2 = new JLabel("Segundo esp\u00EDa");
		lblEspia2.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		lblEspia2.setBounds(10, 49, 114, 21);
		getContentPane().add(lblEspia2);
	}
	
	/**
	 * Crea un combo box y lo coloca en un x predeterminado, pero
	 * con el y pasado por par�metro.
	 */
	private JComboBox<String> comboBoxPredeterminado(int y) {
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setFont(HerramientasInterfaz.fuenteCuadrosDeDialogo());
		comboBox.setBounds(68, y, 153, 18);
		comboBox.addItem("-");
		getContentPane().add(comboBox);
		return comboBox;
	}
	
}
